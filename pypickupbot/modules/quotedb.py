# -*- coding: utf-8 -*-
# pypickupbot - An ircbot that helps game players to play organized games
#               with captain-picked teams.
#     Copyright (C) 2010 pypickupbot authors
#     Copyright (C) 2012-2017 Jan Behrens <zykure42@gmail.com>
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.

"""Topic management commands"""

import sys
from time import sleep
import re
import cgi, urlparse
import random
import hashlib
from datetime import datetime

from twisted.internet import defer, reactor
from twisted.web import server, resource

from pypickupbot.modable import SimpleModuleFactory
from pypickupbot.misc import itime
from pypickupbot import topic
from pypickupbot.irc import COMMAND, InputError
from pypickupbot import config
from pypickupbot import db


_allowed_chars = u"ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789^_-[]ÄÖÜäöüß"

class Quote:
    def __init__(self, index, owner, nick, text, create_dt):
        self.index      = index
        self.owner      = owner
        self.nick       = nick
        self.text       = text
        self.create_dt  = create_dt

    def __str__(self):
        return "Quote by {0}: {1}".format(self.nick, self.text)

############################################################################################################

class QuoteHTTPServer(resource.Resource):
    """Simple HTTP server to publish quotes over the WWW.

    The webserver is built on Twisted's Resource clas.
    """

    isLeaf = True

    startsWithNick = re.compile(r"^<.+> ")

    # Global HTML structure
    htmlOutline = """
        <html>
            <head>
                <title>%(title)s</title>
                <link href='//fonts.googleapis.com/css?family=%(webfont)s' rel='stylesheet'>
                <style type="text/css">
                    body {
                        background: %(bgcolor)s;
                        color: %(fgcolor1)s;
                        margin: 2em 20%%;
                        font-family: %(webfont)s,sans-serif;
                        font-size: 12pt;
                    }
                    a {
                        text-decoration: none;
                        color: %(urlcolor)s;
                    }
                    div.quote {
                        border: 2px solid %(fgcolor1)s;
                        border-top-left-radius: 8px;
                        border-top-right-radius: 8px;
                        margin: 2em 1em;
                    }
                    div.quote .text {
                        clear: both;
                        background-color: %(fgcolor2)s;
                        color: %(fgcolor1)s;
                        padding: 3pt 5pt;
                    }
                    div.quote .header {
                        background-color: %(fgcolor1)s;
                        color: %(fgcolor2)s;
                        padding: 3pt 5pt;
                    }
                    div.quote .header .nick {
                        font-weight: bold;
                    }
                    div.quote .header .info {
                        float:right;
                        color: %(fgcolor2)s;
                        font-size: 9pt;
                        text-align: right;
                    }
                </style>
            </head>
            <body>
                %(body)s
            </body>
        </html>
        """

    def __init__(self, bot):
        self.bot = bot
        self.site = server.Site(self)
        self.handle = reactor.listenTCP(int(config.get("QuoteDB", "http server port").decode('string-escape')), self.site)

    #------------------------------------------------------------------

    def render_GET(self, request):
        split = urlparse.urlsplit(request.uri)
        attrs = urlparse.parse_qs(split.query)

        params = [ x for x in split.path.split("/") if x ]  # combine adjacent separators
        uri = "/".join(params[:1])  # params[1:] -> actual URL params
        baseurl = url = "{0}:{1}".format(config.get("QuoteDB", "http server url"), config.get("QuoteDB", "http server port"))

        request.setHeader("Content-Type", "text/html; charset=utf-8")

        #print params, uri, split, attrs

        if uri in ["add"]:
            return str(self.add_quote( baseurl=baseurl, query=split.query ))

        if uri in [ "", "random" ]:
            if len(params) >= 2:
                limit = int(params[1])
            else:
                limit = int(config.get("QuoteDB", "weblist length"))
            return str(self.print_quotes( baseurl=baseurl, query=split.query, randomize=True, limit=limit ))

        if uri in ["new"]:
            if len(params) >= 2:
                limit = int(params[1])
            else:
                limit = int(config.get("QuoteDB", "weblist length"))
            return str(self.print_quotes( baseurl=baseurl, query=split.query, show_all=True, newest_first=True, limit=limit ))

        if uri in ["all"]:
            return str(self.print_quotes( baseurl=baseurl, query=split.query, show_all=True ))

        if uri in [ "nick" ]:
            if len(params) >= 2:
                nick = params[1].lower()
                return str(self.print_quotes( baseurl=baseurl, query=split.query, show_all=True, nick=nick ))

        if uri in [ "owner" ]:
            if len(params) >= 2:
                owner = params[1].lower()
                return str(self.print_quotes( baseurl=baseurl, query=split.query, show_all=True, owner=owner ))

        if uri in [ "quote" ]:
            if len(params) >= 2:
                try:
                    index = int(params[1])
                except:
                    index = None
                return str(self.print_quotes( baseurl=baseurl, query=split.query, show_all=True, index=index ))

        return self.htmlOutline % {
                'title':        config.get("QuoteDB", "weblist title error"),
                'bgcolor':      config.get("QuoteDB", "weblist background error"),
                'fgcolor1':     config.get("QuoteDB", "weblist color 1"),
                'fgcolor2':     config.get("QuoteDB", "weblist color 2"),
                'urlcolor':     config.get("QuoteDB", "weblist link color"),
                'webfont':      config.get("QuoteDB", "weblist font"),
                'body':         "<h1><a href='/'>QuoteDB</a> (invalid URL)</h1>",
                'script':       "",
                'baseurl':      baseurl,
                'query':        split.query,
            }

    #------------------------------------------------------------------

    def render_POST(self, request):
        split = urlparse.urlsplit(request.uri)
        attrs = urlparse.parse_qs(split.query)

        params = [ x for x in split.path.split("/") if x ]  # combine adjacent separators
        uri = "/".join(params[:1])  # params[1:] -> actual URL params
        baseurl = url = "{0}:{1}".format(config.get("QuoteDB", "http server url"), config.get("QuoteDB", "http server port"))

        if uri == "add":
            args = request.args
            if 'nick' in args and 'key' in args and 'quote' in args:
                nick  = args['nick'][0].strip()
                key   = args['key'][0].strip()
                quote = args['quote'][0].strip()
                if len(nick) > 0 and len(key) == 32 and len(quote) > 0:
                    error_flag = False
                    try:
                        reply = self.bot.add_quote_web(nick, key, quote)
                        content = """
<h1>Success - quote added!</h1>
<div class='quote'><div class='header'>
    <a href="%(url)s/new">Back to QuoteDB</a> | <a href="%(url)s/add">Add another one</a>
</div></div>
""" %                   { 'url': baseurl }
                    except:
                        error_flag = True
                        content = """
<h1>Error - could not add quote!</h1>
<div class='quote'><div class='header'>
    %(reply)s
</div></div>
""" %                   { 'reply': reply }
                    return self.htmlOutline % {
                            'title':        config.get("QuoteDB", "weblist title error" if error_flag else "weblist title"),
                            'bgcolor':      config.get("QuoteDB", "weblist background error" if error_flag else "weblist background"),
                            'fgcolor1':     config.get("QuoteDB", "weblist color 1"),
                            'fgcolor2':     config.get("QuoteDB", "weblist color 2"),
                            'urlcolor':     config.get("QuoteDB", "weblist link color"),
                            'webfont':      config.get("QuoteDB", "weblist font"),
                            'body':         content,
                            'script':       "",
                            'baseurl':      baseurl,
                            'query':        split.query,
                        }

        return self.htmlOutline % {
                'title':        config.get("QuoteDB", "weblist title error"),
                'bgcolor':      config.get("QuoteDB", "weblist background error"),
                'fgcolor1':     config.get("QuoteDB", "weblist color 1"),
                'fgcolor2':     config.get("QuoteDB", "weblist color 2"),
                'urlcolor':     config.get("QuoteDB", "weblist link color"),
                'webfont':      config.get("QuoteDB", "weblist font"),
                'script':       "",
                'baseurl':      baseurl,
                'query':        split.query,
            }

    #------------------------------------------------------------------

    def add_quote(self, baseurl, query):
        content = """
<h1><a href='/'>QuoteDB</a></h1>

<div class='quote'>
    <div class='header'>
        <b><a href="%(url)s/add">Add a new quote to the DB!</a></b><br/>
        Use <i>!quotekey</i> without arguments in IRC to get a personal key for adding quotes here.
    </div>
    <div class='text'>
        <form action="%(url)s/add" method="post">
            <p>
                Key:<br/>
                <input type="text" name="key" id="key" size="50" maxlength="40"/>
            </p>
            <p>
                Nick:<br/>
                <input type="text" name="nick" id="nick" size="50" maxlength="20"/>
            </p>
            <p>
                Quote:<br/>
                <textarea name="quote" id="quote" rows="8" cols="70" wrap="soft" style="padding:8px;"></textarea>
            </p>
            <input type="submit" value="add!"><br/>
        </form>
    </div>
</div>
""" % { 'url': baseurl }
        return self.htmlOutline % {
                'title':        config.get("QuoteDB", "weblist title"),
                'bgcolor':      config.get("QuoteDB", "weblist background"),
                'fgcolor1':     config.get("QuoteDB", "weblist color 1"),
                'fgcolor2':     config.get("QuoteDB", "weblist color 2"),
                'urlcolor':     config.get("QuoteDB", "weblist link color"),
                'webfont':      config.get("QuoteDB", "weblist font"),
                'body':         content,
                'script':       "",
                'baseurl':      baseurl,
                'query':        query,
            }

    #------------------------------------------------------------------

    def print_quotes(self, baseurl, query, show_all=False, nick=None, owner=None, index=None, \
            limit=None, randomize=False, newest_first=False):
        title = ""
        if randomize:
            title = "random quotes: "
        if show_all:
            title = "all quotes: "
        if nick:
            title = "quotes for %s: " % nick
        elif owner:
            title = "quotes by %s: " % owner
        elif index:
            title = "quote #%d: " % index

        flag = ""
        keys = sorted(self.bot.quotes.keys(), reverse=newest_first)
        quotes = []

        if len(self.bot.quotes) > 0:
            if randomize:
                random.shuffle(keys)
                quotes = [ self.bot.quotes[k] for k in keys ]
                if limit:
                    quotes = quotes[:limit]
            elif index != None:
                if index in self.bot.quotes:
                    quotes.append( self.bot.quotes[index] )
            else:
                if limit:
                    keys = keys[:limit]
                for k in keys:
                    q = self.bot.quotes[k]
                    if nick and not q.nick.lower() == nick:
                        continue
                    if owner and not q.owner.lower() == owner:
                        continue
                    quotes.append(q)

        content = ""
        if len(quotes) == 0:
            content += "<h1><a href='/'>QuoteDB</a> <small>(%snothing found!)</small></h1>" % title
        elif len(quotes) == 1:
            content += "<h1><a href='/'>QuoteDB</a> <small>(%sshowing 1 entry of %d)</small></h1>" % (title, len(self.bot.quotes))
        else:
            content += "<h1><a href='/'>QuoteDB</a> <small>(%sshowing %d entries of %d)</small></h1>" % (title, len(quotes), len(self.bot.quotes))

        for quote in quotes:
            # add <nick> before each line if it is missing in every line in the text
            # TODO parse & remove timestamps
            # TODO allow playernicks too (more characters than IRC nicks)
            addNick = True
            quoteLines = quote.text.split(" | ")
            for line in quoteLines:
                if self.startsWithNick.match(line):
                    addNick = False
                    break

            text = []
            for line in quoteLines:
                if addNick:
                    line = "<%s> %s" % ( quote.nick, line )
                text.append(cgi.escape(line))

            content += """
<div class='quote'>
    <div class='header'>
        <span class='nick'><a href='%(url)s/nick/%(nick)s'>%(nick)s</a></span>
        <span class='info'>Quote <a href='%(url)s/quote/%(index)s'>#%(index)s.</a> by <a href='%(url)s/owner/%(owner)s'>%(owner)s</a> at %(time)s <small>&emsp;[<a href='%(url)s/random'>random</a>|<a href='%(url)s/new'>latest</a>|<a href='%(url)s/all'>all</a>]</small></span>
    </div>
    <div class='text'>%(text)s</div>
</div>
""" %   {
            'url':      baseurl,
            'index':    quote.index,
            'nick':     cgi.escape(quote.nick.encode('utf-8', errors='ignore')),
            'text':     "<br/>".join(text).encode('utf-8', errors='ignore'),
            'time':     datetime.fromtimestamp(quote.create_dt).strftime("%Y-%m-%d %H:%M"),
            'owner':    cgi.escape(quote.owner.encode('utf-8', errors='ignore')),
        }

        content += """
<div style='text-align:right;'><a href='%(url)s/add'>Add another one!</a></div>
""" %   { 'url': baseurl }

        return self.htmlOutline % {
                'title':        config.get("QuoteDB", "weblist title"),
                'bgcolor':      config.get("QuoteDB", "weblist background"),
                'fgcolor1':     config.get("QuoteDB", "weblist color 1"),
                'fgcolor2':     config.get("QuoteDB", "weblist color 2"),
                'urlcolor':     config.get("QuoteDB", "weblist link color"),
                'webfont':      config.get("QuoteDB", "weblist font"),
                'body':         content,
                'script':       "",
                'baseurl':      baseurl,
                'query':        query,
            }

############################################################################################################

class QuoteDB:

    SPAM_TIMEOUT        = 10
    MULTILINE_DELAY     = 0.2

    def __init__(self, bot, cback=None):
        self.pypickupbot = bot
        self.lastspam = 0
        self.quotes   = {}
        self.keys     = {}  # nick - key

        def _doTransaction(txn):
            txn.execute("""
                CREATE TABLE IF NOT EXISTS
                quotesdb
                (
                    id      INTEGER PRIMARY KEY AUTOINCREMENT,
                    nick    TEXT,
                    time    INTEGER,
                    owner   TEXT,
                    text    TEXT
                )""")
            txn.execute("""
                DROP TABLE IF EXISTS quotesdb_fts
                """)
            txn.execute("""
                CREATE VIRTUAL TABLE
                quotesdb_fts USING fts4
                (
                    nick    TEXT,
                    text    TEXT,
                )
                """)
            txn.execute("""
                INSERT INTO quotesdb_fts(nick,text)
                SELECT      nick,text
                FROM        quotesdb
                """)
            return txn.fetchall()

        def done(*args):
            if cback:
                self._load_from_db().addCallback(cback)
            else:
                self._load_from_db()
        db.runInteraction(_doTransaction).addCallback(done)

        try:
            self.httpServer.handle.startListening();
        except:
            self.httpServer = QuoteHTTPServer(self)

    def _load_from_db(self):
        """used by this class to load all single quotes from the db."""
        d = db.runQuery("""
            SELECT id,nick,time,owner,text
            FROM quotesdb
            ORDER BY id
            """)
        def _loadResults(r):
            self.quotes = {}
            for entry in r:
                index, nick, create_dt, owner, text = entry
                self.quotes[index] = Quote(index, owner, nick, text, create_dt)
        return d.addCallback(_loadResults)

    def _check_spam(self):
        now = itime()
        delta = now - self.lastspam
        self.lastspam = now  # reset spamcounter
        return (delta > self.SPAM_TIMEOUT)

    def _purge(self, keep=0):
        """used by clearQuotes and purgeQuotes"""
        d = db.runOperation("""
                DELETE FROM quotesdb
                WHERE  time<?
                """, (itime() - keep,))
        return d

    def _purgenick(self, nick, keep=0):
        """used by purgeNick"""
        d = db.runOperation("""
                DELETE FROM quotesdb
                WHERE  time<?
                  AND  nick=?
                """, (itime() - keep, nick, ))
        return d

    def _deleteIndex(self, index):
        """used by delquote"""
        def _doTransaction(txn, index):
            txn.execute("""
                    DELETE FROM quotesdb
                    WHERE  id=?
                    """, (index, ))
            #txn.execute("""
            #        DELETE FROM quotesdb_fts
            #        WHERE  id=?
            #        """, (index, ))
            return txn.fetchall()
        return db.runInteraction(_doTransaction, index)

    def _delete(self, nick, text):
        """used by delquote"""
        def _doTransaction(txn, nick, text):
            txn.execute("""
                    DELETE FROM quotesdb
                    WHERE  nick=?
                      AND  text=?
                    """, (nick, text, ))
            txn.execute("""
                    DELETE FROM quotesdb_fts
                    WHERE  nick=?
                      AND  text=?
                    """, (nick, text, ))
            return txn.fetchall()
        return db.runInteraction(_doTransaction, nick, text)

    def _insert(self, nick, text, owner):
        def _doTransaction(txn, nick, text, owner):
            txn.execute("""
                INSERT INTO quotesdb(nick, text, time, owner)
                VALUES (?, ?, ?, ?)
                """, (nick, text, itime(), owner, ))
            txn.execute("""
                INSERT INTO quotesdb_fts(nick, text)
                VALUES (?, ?)
                """, (nick, text, ))
            return txn.fetchall()
        return db.runInteraction(_doTransaction, nick, text, owner)

    def _retrieve_all(self, nick=None):
        if nick:
            d = db.runQuery("""
                SELECT nick, text
                FROM   quotesdb
                WHERE  nick=?
                ORDER BY time
            """, (nick,))
        else:
            d = db.runQuery("""
                SELECT nick, text
                FROM   quotesdb
                ORDER BY time
            """)
        return d

    def _retrieve(self, nick=None):
        if nick:
            d = db.runQuery("""
                SELECT nick, text
                FROM   quotesdb
                WHERE  nick=?
                ORDER BY random()
                LIMIT  1
            """, (nick,))
        else:
            d = db.runQuery("""
                SELECT nick, text
                FROM   quotesdb
                ORDER BY random()
                LIMIT  1
            """)
        return d

    def _listall(self, nick=None):
        if nick:
            d = db.runQuery("""
                SELECT nick, count(nick)
                FROM   quotesdb
                WHERE  nick=?
                GROUP  BY nick
            """, (nick,))
        else:
            d = db.runQuery("""
                SELECT nick, count(nick)
                FROM   quotesdb
                GROUP  BY nick
            """)
        return d

    def _search(self, phrase):
        d = db.runQuery("""
            SELECT nick, text
            FROM   quotesdb_fts
            WHERE  text MATCH ?
            ORDER BY random()
            LIMIT  1
        """, ('"'+phrase+'"',))
        return d

    def add_quote_web(self, nick, key, quote):
        owner = None
        for n,k in self.keys.items():
            if k == key:
                owner = n
                break
        if not owner:
            return _("This is not a valid key! Use !quotekey in IRC to get one.")

        nick = nick.strip()
        if not all(c in _allowed_chars for c in nick):
            return _("Please use only 'simple' characters in nicks.")

        text = []
        for line in quote.splitlines():  # fix lines
            line = line.strip()
            if len(line) > 0:
                text.append(line)
        text = " | ".join(text)

        max_length = config.getint("QuoteDB", "maximum quote length")
        if len(text) > max_length:
            return _("Your quote is larger than the allowed length of {0} characters (+{1} chars)!").format(max_length, len(text)-max_length)

        def done(args):
            def done(args):
                self.pypickupbot.fire('quote_added', nick, owner, "web")
            self._load_from_db().addCallback(done)
        self._insert(nick.lower(), text, owner).addCallback(done)
        return _("Quote added")

    def clearQuotes(self, call, args):
        """!clearquotes [nick]

        Clears the quotes db."""
        nick = None
        if len(args) > 0:
            nick = args[0].strip().lower()

        if nick:
            d = call.confirm(_("This will delete all quotes by \x02{0}\x02 from the database, continue?").format(nick))
            def _confirmed(ret):
                if ret:
                    def done(*args):
                        def done(args):
                            call.reply(_("Done."))
                        self._load_from_db().addCallback(done)
                    return self._purgenick(nick).addCallback(done)
                else:
                    call.reply(_("Cancelled."))
            return d.addCallback(_confirmed)
        else:
            d = call.confirm(_("This will delete all quotes from the database, continue?"))
            def _confirmed(ret):
                if ret:
                    def done(*args):
                        def done(args):
                            call.reply(_("Done."))
                        self._load_from_db().addCallback(done)
                    return self._purge().addCallback(done)
                else:
                    call.reply(_("Cancelled."))
            return d.addCallback(_confirmed)

    def getQuoteKey(self, call, args):
        """!quotekey

        Retrieve key to add quotes via webinterface. Only use the key to add quotes yourself!"""

        nick = call.nick
        salt = "%s!%ld" % (nick, random.getrandbits(64))
        key = hashlib.md5(salt).hexdigest()
        print nick, salt, key
        self.keys[nick] = key
        call.reply( _("{0}: Your personal key to add quotes is: {1}").format(nick,key))

    def addQuote(self, call, args):
        """!addquote <nick> <text>

        Add quote from <nick> to the quote db.
        """
        if len(args) < 2:
            call.reply(_("You have to specify a nick and a quote text!"))
            return

        nick = args[0].strip()
        if not all(c in _allowed_chars for c in nick):
            call.reply(_("Please use only 'simple' characters in nicks."))
            return

        text = u" ".join([ s.decode("utf-8", errors='ignore') for s in args[1:] ])
        #text = u" ".join(args[1:]).encode('utf-8', errors='ignore')
        max_length = config.getint("QuoteDB", "maximum quote length")
        if len(text) > max_length:
            call.reply(_("Your quote is larger than the allowed length of {0} characters (+{1} chars)!").format(max_length, len(text)-max_length))
            return

        #d = call.confirm(_(u"Add quote by \x02\x0302{0}\x0f: \x0305{1}\x0f?").format(nick, text))
        def _confirmed(ret):
            if ret:
                def done(args):
                    def done(args):
                        self.pypickupbot.fire('quote_added', nick, call.nick, call.channel)
                        call.reply(_("Thanks!"))
                    self._load_from_db().addCallback(done)
                self._insert(nick.lower(), text, call.nick).addCallback(done)
            else:
                call.reply(_("Cancelled."))
        #return d.addCallback(_confirmed)
        _confirmed(True)  # do not require extra !yes

    def delQuote(self, call, args):
        """!delquote [<nick> <text>|#<index>]

        Removes quote from <nick> from the quote db.
        """
        index = None
        if len(args) == 1:
            if args[0].startswith("#"):
                index = int(args[0][1:])
        else:
            if len(args) < 2:
                call.reply(_("You have to specify a nick and a quote text!"))
                return

        if index:
            if not index in self.quotes:
                call.reply(_("This quote doesn't exist!"))
                return

            quote = self.quotes[index]

            d = call.confirm(_(u"Remove quote \x0305#{0}\x0f by \x02\x0302{1}\x0f?").format(index, quote.nick))
            def _confirmed(ret):
                if ret:
                    def done(args):
                        def done(args):
                            call.reply(_("Done."))
                        self._load_from_db().addCallback(done)
                    self._deleteIndex(index).addCallback(done)
                else:
                    call.reply(_("Cancelled."))
            return d.addCallback(_confirmed)
        else:
            nick = args[0].strip()
            if not all(c in _allowed_chars for c in nick):
                call.reply(_("Please use only 'simple' characters in nicks."))
                return

            #text = u" ".join([s.decode("utf-8") for s in args[1:]])
            text = u" ".join(args[1:])

            d = call.confirm(_(u"Remove quote by \x02\x0302{0}\x0f, if it exists: \x0305{1}\x0f?").format(nick, text))
            def _confirmed(ret):
                if ret:
                    def done(args):
                        def done(args):
                            call.reply(_("Done."))
                        self._load_from_db().addCallback(done)
                    self._delete(nick.lower(), text).addCallback(done)
                else:
                    call.reply(_("Cancelled."))
            return d.addCallback(_confirmed)

    def quote(self, call, args):
        """!quote [nick]

        Show random quote. If <nick> is given, only show quotes corresponding to that name.
        """
        if not self._check_spam():
            call.reply(_("Don't spam!"))
            return

        query = None
        if len(args) > 0:
            query = args[0].strip().lower()

        #def done(r):
        #    if len(r) < 1:
        #        call.reply(_("No quotes found!"))
        #    else:
        #        for entry in r[:1]:  # single quote
        #            nick, text = entry
        #        lines = text.split(" | ")
        #        msg = config.get('QuoteDB', 'quote').decode('string-escape')%\
        #            {
        #                'nick':     nick,
        #                'quote':    lines[0].strip()
        #            }
        #        self.pypickupbot.cmsg(msg, channel=call.channel)
        #        for line in lines[1:]:
        #            sleep(self.MULTILINE_DELAY)
        #            msg = config.get('QuoteDB', 'quote morelines').decode('string-escape')%{ 'quote': line.strip() }
        #            self.pypickupbot.cmsg(msg, channel=call.channel)
        #self._retrieve(query).addCallback(done)

        if len(self.quotes) < 1:
            call.reply(_("No quotes found!"))
        else:
            quotes = []
            for q in self.quotes.values():
                if not query or q.nick == query.lower():
                    quotes.append(q)
            quote = random.choice(quotes)
            lines = quote.text.split(" | ")
            msg = config.get('QuoteDB', 'quote').decode('string-escape')%\
                {
                    'nick':     quote.nick,
                    'quote':    lines[0].strip()
                }
            self.pypickupbot.cmsg(msg, channel=call.channel)
            for line in lines[1:]:
                sleep(self.MULTILINE_DELAY)
                msg = config.get('QuoteDB', 'quote morelines').decode('string-escape')%{ 'quote': line.strip() }
                self.pypickupbot.cmsg(msg, channel=call.channel)

    def listQuotes(self, call, args):
        """!listquotes [nick]

        Show number of quotes in the db for given nick, or all nicks if no argument is provided.
        """
        nick = None
        if len(args) > 0:
            nick = args[0].strip().lower()

        #def done(r):
        #    if len(r) < 1:
        #        call.reply(_("No quotes found!"))
        #    else:
        #        quotes = {}
        #        for entry in r:
        #            nick, count = entry
        #            quotes[nick] = count
        #        if not query:
        #            msg = config.get('QuoteDB', 'quote list').decode('string-escape')%\
        #                {
        #                    'total':    sum(quotes.values()),
        #                    'quotes':   ", ".join([ config.get('QuoteDB', 'quote list entry').decode('string-escape')%\
        #                            {
        #                                'nick':     nick,
        #                                'count':    quotes[nick],
        #                            }
        #                            for nick in sorted(quotes.keys()) ]),
        #                }
        #        else:
        #            msg = config.get('QuoteDB', 'quote list single').decode('string-escape')%\
        #                {
        #                    'total':    sum(quotes.values()),
        #                    'nick':     nick,
        #                }
        #        call.reply(msg)
        #self._listall(query).addCallback(done)

        if len(self.quotes) < 1:
            call.reply(_("No quotes found!"))
        else:
            # get quote counts
            quotes = {}
            for quote in self.quotes.values():
                if not quote.nick in quotes:
                    quotes[quote.nick] = 0
                quotes[quote.nick] += 1
            if not nick:
                msg = config.get('QuoteDB', 'quote list').decode('string-escape')%\
                    {
                        'total':    sum(quotes.values()),
                        'quotes':   ", ".join([ config.get('QuoteDB', 'quote list entry').decode('string-escape')%\
                                {
                                    'nick':     nick,
                                    'count':    quotes[nick],
                                }
                                for nick in sorted(quotes.keys()) ]),
                    }
            else:
                msg = config.get('QuoteDB', 'quote list single').decode('string-escape')%\
                    {
                        'total':    sum(quotes.values()),
                        'nick':     nick,
                    }
            call.reply(msg)

    def findQuote(self, call, args):
        """!findquote <phrase>

        Show random quote which contains the given phrase.
        """

        ## TODO decide wether to use direct DB access or cached quote dict

        if len(args) < 1:
            call.reply(_("You must specify a phrase to look for!"))
            return

        query = " ".join(args)

        def done(r):
            if len(r) < 1:
                call.reply(_("No quotes found!"))
            else:
                for entry in r[:1]:  # single quote
                    nick, text = entry
                lines = text.split(" | ")
                msg = config.get('QuoteDB', 'quote').decode('string-escape')%\
                    {
                        'nick':     nick,
                        'quote':    lines[0].strip(),
                    }
                self.pypickupbot.cmsg(msg, channel=call.channel)
                for line in lines[1:]:
                    sleep(self.MULTILINE_DELAY)
                    msg = config.get('QuoteDB', 'quote morelines').decode('string-escape')%{ 'quote': line.strip() }
                    self.pypickupbot.cmsg(msg, channel=call.channel)
        self._search(query).addCallback(done)

    def quoteAdded(self, nick, owner, via):
        msg = _("\x0313Quote for \x0302{0}\x0313 added by {1} \x0315(via {2})\x0313!").format(nick, owner, via)
        self.pypickupbot.cmsg(msg)

    commands = {
        'addquote':     (addQuote,      0 ),
        'delquote':     (delQuote,      COMMAND.NOT_FROM_PM | COMMAND.ADMIN ),
        'quote':        (quote,         COMMAND.NOT_FROM_PM ),
        'findquote':    (findQuote,     COMMAND.NOT_FROM_PM),
        'listquotes':   (listQuotes,    0),
        'quotekey':     (getQuoteKey,   0),

        'clearquotes':  (clearQuotes,   COMMAND.NOT_FROM_OUTSIDE | COMMAND.ADMIN),
    }

    eventhandlers = {
        'quote_added':  quoteAdded,
        }

qupte_db = SimpleModuleFactory(QuoteDB)
