# pypickupbot - An ircbot that helps game players to play organized games
#               with captain-picked teams.
#     Copyright (C) 2010 pypickupbot authors
#     Copyright (C) 2012-2017 Jan Behrens <zykure42@gmail.com>
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.

"""Pickup-related commands"""
from time import time, sleep
from datetime import datetime
import subprocess
import re
import socket
import sys, os

from twisted.python import log
from twisted.internet import defer

from pypickupbot.modable import SimpleModuleFactory
from pypickupbot.irc import COMMAND, InputError, FetchedList
from pypickupbot.topic import Topic
from pypickupbot import db
from pypickupbot import config
from pypickupbot.misc import str_from_timediff, timediff_from_str,\
    InvalidTimeDiffString, StringTypes, itime
from pypickupbot.pygeoip import Database


GEOIP = Database(os.path.dirname(sys.argv[0]) + "/GeoIP.dat")


# Map of special chars to ascii from Darkplace's console.c.
_qfont_table = [
 '\0', '#',  '#',  '#',  '#',  '.',  '#',  '#',
 '#',  '\t', '\n', '#',  ' ',  '\r', '.',  '.',
 '[',  ']',  '0',  '1',  '2',  '3',  '4',  '5',
 '6',  '7',  '8',  '9',  '.',  '<',  '=',  '>',
 ' ',  '!',  '"',  '#',  '$',  '%',  '&',  '\'',
 '(',  ')',  '*',  '+',  ',',  '-',  '.',  '/',
 '0',  '1',  '2',  '3',  '4',  '5',  '6',  '7',
 '8',  '9',  ':',  ';',  '<',  '=',  '>',  '?',
 '@',  'A',  'B',  'C',  'D',  'E',  'F',  'G',
 'H',  'I',  'J',  'K',  'L',  'M',  'N',  'O',
 'P',  'Q',  'R',  'S',  'T',  'U',  'V',  'W',
 'X',  'Y',  'Z',  '[',  '\\', ']',  '^',  '_',
 '`',  'a',  'b',  'c',  'd',  'e',  'f',  'g',
 'h',  'i',  'j',  'k',  'l',  'm',  'n',  'o',
 'p',  'q',  'r',  's',  't',  'u',  'v',  'w',
 'x',  'y',  'z',  '{',  '|',  '}',  '~',  '<',

 '<',  '=',  '>',  '#',  '#',  '.',  '#',  '#',
 '#',  '#',  ' ',  '#',  ' ',  '>',  '.',  '.',
 '[',  ']',  '0',  '1',  '2',  '3',  '4',  '5',
 '6',  '7',  '8',  '9',  '.',  '<',  '=',  '>',
 ' ',  '!',  '"',  '#',  '$',  '%',  '&',  '\'',
 '(',  ')',  '*',  '+',  ',',  '-',  '.',  '/',
 '0',  '1',  '2',  '3',  '4',  '5',  '6',  '7',
 '8',  '9',  ':',  ';',  '<',  '=',  '>',  '?',
 '@',  'A',  'B',  'C',  'D',  'E',  'F',  'G',
 'H',  'I',  'J',  'K',  'L',  'M',  'N',  'O',
 'P',  'Q',  'R',  'S',  'T',  'U',  'V',  'W',
 'X',  'Y',  'Z',  '[',  '\\', ']',  '^',  '_',
 '`',  'a',  'b',  'c',  'd',  'e',  'f',  'g',
 'h',  'i',  'j',  'k',  'l',  'm',  'n',  'o',
 'p',  'q',  'r',  's',  't',  'u',  'v',  'w',
 'x',  'y',  'z',  '{',  '|',  '}',  '~',  '<'
]

def qfont_decode(qstr=''):
    """ Convert the qfont characters in a string to ascii.
    """
    if qstr == None:
        qstr = ''
    chars = []
    for c in qstr:
        if u'\ue000' <= c <= u'\ue0ff':
            c = _qfont_table[ord(c) - 0xe000]
        chars.append(c)
    return ''.join(chars).replace('\x00','')


def irc_colors(qstr, bold=False):
    _irc_colors = [ -1, 4, 9, 8, 12, 11, 13, -1, -1, -1 ]
    def _rgb_to_simple(r,g,b):
        # this was basically taken from rcon2irc.pl
        min_ = min(r,g,b)
        max_ = max(r,g,b)

        v = max_ / 15.0
        s = (1 - min_/max_) if max_ != min_ else 0

        if s < 0.2:
            return 0 if v < 0.5 else 7

        if max_ == min_:
            h = 0
        elif max_ == r:
            h = (60 * (g - b) / (max_ - min_)) % 360
        elif max_ == g:
            h = (60 * (b - r) / (max_ - min_)) + 120
        elif max_ == b:
            h = (60 * (r - g) / (max_ - min_)) + 240

        if h < 36:
            return 1
        elif h < 80:
            return 3
        elif h < 150:
            return 2
        elif h < 200:
            return 5
        elif h < 270:
            return 4
        elif h < 330:
            return 6
        else:
            return 1

    _all_colors = re.compile(r'(\^\d|\^x[\dA-Fa-f]{3})')
    #qstr = ''.join([ x if ord(x) < 128 else '' for x in qstr ]).replace('^^', '^').replace(u'\x00', '') # strip weird characters
    parts = _all_colors.split(qstr)
    result = "\002"
    oldcolor = None
    while len(parts) > 0:
        tag = None
        txt = parts[0]
        if _all_colors.match(txt):
            tag = txt[1:]  # strip leading '^'
            if len(parts) < 2:
                break
            txt = parts[1]
            del parts[1]
        del parts[0]

        if not txt or len(txt) == 0:
            # only colorcode and no real text, skip this
            continue

        color = 7
        if tag:
            if len(tag) == 4 and tag[0] == 'x':
                r = int(tag[1], 16)
                g = int(tag[2], 16)
                b = int(tag[3], 16)
                color = _rgb_to_simple(r,g,b)
            elif len(tag) == 1 and int(tag[0]) in range(0,10):
                color = int(tag[0])
        color = _irc_colors[color]
        if color != oldcolor:
            if color < 0:
                result += "\017\002"
            else:
                result += "\003" + "%02d" % color
        result += txt
        oldcolor = color
    result += "\017"
    return result

#######################################################################################################################

class ServerPlayer:

    def __init__(self, nick, score=-666, ping=-1):
        self.nick  = nick
        self.score = score
        self.ping  = ping

    def __str__(self):
        return qfont_decode(self.nick)

class Server:

    SOCKET_TIMEOUT = 0.5

    def __init__(self, ip, name, index=-1):
        """ip   = IP address of server (IPV6 support??)
           name = Short identifier of the server for use in the channel (e.g. dccminsta, extdm1 etc.)"""
        self.ip             = ip
        self.name           = name
        self.index          = index
        self.update         = None

        self.valid          = False
        self.available      = False
        self.status         = ""
        self.ping           = -1
        self.gamename       = ""
        self.modname        = ""
        self.gameversion    = ""
        self.maxclients     = 0
        self.clients        = 0
        self.bots           = 0
        self.mapname        = ""
        self.hostname       = ""
        self.protocol       = ""
        self.qcstatus       = ""
        self.gametype       = ""
        self.pure_flags     = 0
        self.slots          = 0
        self.server_flags   = 0
        self.mutator        = 0
        self.d0_blind_id    = ""
        self.maxplayers     = 0
        self.numplayers     = 0
        self.numclients     = 0
        self.numspectators  = 0
        self.players        = []
        self.spectators     = []

        self.sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
        self.sock.settimeout(self.SOCKET_TIMEOUT)

    def __str__(self):
        return "{0}".format(self.name)

    def _update(self):
        if self.update and (abs(time()-self.update) < int(config.get('GameServer Interface', 'query cache livetime').decode('string-escape'))):
            log.msg("Using cached query for server {0}".format(self.name))
            return True

        self.valid          = False
        self.available      = False
        self.status         = ""
        self.ping           = -1
        self.gamename       = ""
        self.modname        = ""
        self.gameversion    = ""
        self.maxclients     = 0
        self.clients        = 0
        self.bots           = 0
        self.mapname        = ""
        self.hostname       = ""
        self.protocol       = ""
        self.qcstatus       = ""
        self.gametype       = ""
        self.impure_flags   = 0
        self.slots          = 0
        self.server_flags   = 0
        self.mutator        = ""
        self.d0_blind_id    = ""
        self.maxplayers     = 0
        self.numplayers     = 0
        self.numclients     = 0
        self.numspectators  = 0
        self.players        = []
        self.spectators     = []

        self.update = time()

        log.msg("Querying server {0} ({1})".format(self.name, self.ip))

        try:
            address = ( self.ip.split(":")[0], int(self.ip.split(":")[1]) )
            self.sock.sendto("\xFF\xFF\xFF\xFFgetstatus", address)
            data = self.sock.recvfrom(2048)[0].split("\n")
            # data[0] is command
            status  = data[1].split("\\")
            players = data[2:]
            self.valid = True
        except:
            log.msg("Error: {0}!".format(sys.exc_info()[0]))
            return False

        self.status = status
        idx = 0
        while idx+1 < len(status):
            key, value = status[idx], status[idx+1]
            if key == "gamename":
                self.gamename = value
            elif key == "modname":
                self.modname = value
            elif key == "gameversion":
                self.gameversion = value
            elif key == "sv_maxclients":
                self.maxclients = int(value)
            elif key == "clients":
                self.clients = int(value)
            elif key == "bots":
                self.bots  = int(value)
            elif key == "mapname":
                self.mapname = value
            elif key == "hostname":
                self.hostname = value
            elif key == "protocol":
                self.protocol = value
            elif key == "qcstatus":
                self.qcstatus = value.split(":")
                if len(self.qcstatus) >= 6:
                    self.gametype       = self.qcstatus[0]
                    self.gameversion    = self.qcstatus[1]
                    if self.qcstatus[2].startswith("P"):
                        self.impure_flags   = int(self.qcstatus[2][1:])
                    if self.qcstatus[3].startswith("S"):
                        self.slots          = int(self.qcstatus[3][1:])
                    if self.qcstatus[4].startswith("F"):
                        self.server_flags   = int(self.qcstatus[4][1:])
                    self.mutator        = self.qcstatus[5]
            elif key == "d0_blind_id":
                self.d0_blind_id = value
            else:  # wrong key position, try next field
                idx += 1
                continue
            idx += 2

        self.players    = []
        self.spectators = []
        self.ping = 0
        for row in players:
            if len(row) == 0:
                continue
            fields = row.decode('utf-8', errors='ignore').split(" ")
            #fields = row.split(" ")
            try:
                score = int(float(fields[0]))
                ping  = int(float(fields[1]))
                num   = int(float(fields[2]))
            except:
                score = 0
                ping  = 0
                num   = None
            if num == None:  # fields 2+ contains nick
                nick  = u" ".join(fields[2:]).strip()
            else:  # field 2 contains number
                nick  = u" ".join(fields[3:]).strip()
            if nick.startswith(u"\"") and nick.endswith(u"\""):  # remove quotation marks
                nick = nick[1:-1]
            if score == -666:  # spectators have this score per def.
                self.spectators.append( ServerPlayer(nick,score,ping) )
            else:
                self.players.append( ServerPlayer(nick,score,ping) )
            self.ping += ping
        self.ping /= len(players) if len(players) > 0 else 1

        self.numplayers    = len(self.players)
        self.numspectators = len(self.spectators)
        self.numclients    = self.numplayers + self.numspectators
        self.maxplayers    = self.numplayers + self.bots + self.slots

        self.available = True
        return True

    def get_ping(self):
        # return current ping to the server
        self._update()
        return self.ping

    def get_location(self):
        # return location of the server (could use geoip methods)
        info = GEOIP.lookup(self.ip.split(":")[0])  # split port
        if info.country:
            return info.country
        return "??"

    def get_map(self):
        # return current map on the server
        self._update()
        return self.mapname

    def num_clients(self):
        # return current number of clients on the server
        self._update()
        return self.numclients

    def num_players(self):
        # return current number of players on the server
        self._update()
        return self.numplayers

    def num_bots(self):
        # return current number of bots on the server
        self._update()
        return self.bots

    def num_spectators(self):
        # return current number of spectators on the server
        self._update()
        return self.numspectators

    def max_clients(self):
        # return maximum number of players on the server
        self._update()
        return self.clients

    def max_players(self):
        # return maximum number of players on the server
        self._update()
        return self.maxplayers

    def free_slots(self):
        # return currently available free slots on the server
        self._update()
        return self.slots

    def is_full(self):
        return (self.free_slots() == 0)

    def is_empty(self):
        return (self.num_players() == 0)

    def is_pure(self):
        return (self.impure_flags == 0)

    def is_valid(self):
        # check if server is a valid game server
        self._update()
        return self.valid

    def is_private(self):
        return True

    def is_public(self):
        return (not self.is_private())

    def is_available(self):
        # check if server is currently available
        self._update()
        return self.available

    def get_gametypes(self):
        # get supported gametypes as stringlist
        return []

    def get_mutators(self):
        # get active mutators as stringlist (?)
        return []

    def supports_gametype(self, gametype):
        # check if server supports given gametype
        return False

    def get_ip(self):
        return self.ip

    def get_name(self):
        return self.name

    def get_fullname(self):
        self._update()
        return self.hostname

    def get_qcstatus(self):
        self._update()
        return self.qcstatus

    def get_gamename(self):
        self._update()
        return self.gamename

    def get_gameversion(self):
        self._update()
        return self.gameversion

    def get_bots(self):
        self._update()
        return self.bots

    def get_gametype(self):
        self._update()
        return self.gametype

    def get_gamemod(self):
        self._update()
        if self.mutator.startswith("M"):
            return self.mutator[1:]  # first letter is always "M"
        else:
            return "??"

    def get_players(self):
        players = []
        self._update()
        for p in self.players:
            players.append(p.nick)
        return players

    def get_playerscores(self):
        playerscores = {}
        self._update()
        for p in self.players:
            playerscores[p.nick] = p.score
        return playerscores

    def get_spectators(self):
        spectators = []
        self._update()
        for p in self.spectators:
            spectators.append(p.nick)
        return spectators

#######################################################################################################################

class GameServerInterface:

    def __init__(self):
        self.servers = {}       # name : item

        def _done(args):
            self._load_from_db()
        d = db.runOperation("""
            CREATE TABLE IF NOT EXISTS
            game_servers
            (
                id          INTEGER PRIMARY KEY AUTOINCREMENT,
                name        TEXT,
                ipaddress   TEXT,
                create_dt   INTEGER
            )""")
        d.addCallback(_done)

    def _load_from_db(self):
        d = db.runQuery("""
            SELECT      id, name, ipaddress, create_dt
            FROM        game_servers
            ORDER BY    create_dt
            """)
        self.servers = {}
        def _loadServers(r):
            for entry in r:
                index, name, ip, create_dt = entry
                self.servers[name] = Server(ip, name, index)
        return d.addCallback(_loadServers)

    def _find_server(self, name):
        if self.servers.has_key(name):
            return self.servers[name]
        return None

    def _find_serverip(self, serverip):
        result = {}
        for k,s in self.servers.items():
            if s.get_ip().startswith(serverip):
                result[k] = s
        return result

    def _search(self, string):
        result = {}
        for k,s in self.servers.items():
            if string.lower() in s.get_fullname().lower():
                result[k] = s
        return result

    def _purge(self, keep=0):
        """used by clearServers and purgeServers"""
        d = defer.gatherResults(
            db.runOperation("""
                DELETE FROM game_servers
                WHERE       create_dt < ?
                """, (itime() - keep,))
            )
        def onErr(failure):
            log.err(failure, "purge servers, keep = {0}".format(keep))
            return failure
        d.addErrback(onErr)
        return d

    def _delete(self, name):
        if name.startswith("#"):
            index = int(name[1:])
            return db.runOperation("""
                DELETE FROM game_servers
                WHERE       id=?
                """, (index,))
        else:
            return db.runOperation("""
                DELETE FROM game_servers
                WHERE       name=?
                """, (name,))

    def _insert(self, serverip, name):
        return db.runOperation("""
            INSERT INTO game_servers(name, ipaddress, create_dt)
            VALUES      (:name, :ipaddress, :ctime)
            """, (name, serverip, itime(),))

#######################################################################################################################

class PickupGameServerBot:

    def _check_timer(self):
        return True

        # not working as expected
        now = time()
        if self.lastcmd and abs(now-self.lastcmd) < int(config.get("GameServer Interface", "min interval between commands").decode('string-escape')):
            return False
        self.lastcmd = now
        return True

    def clearServers(self, call, args):
        """!clearservers

        Clears the registered servers list completely. Prefer the purgeservers command to this."""
        d = call.confirm("This will delete all registered servers, continue?")
        def _confirmed(ret):
            if ret:
                def done(*args):
                    call.reply(_("Done."))
                return self.servers._purge().addCallback(done)
            else:
                call.reply(_("Cancelled."))
        return d.addCallback(_confirmed)

    def purgeServers(self, call, args):
        """!purgeservers <keep>

        Purges the registered servers list from entries created later than [keep] ago."""
        if not len(args) == 1:
            call.reply(_("You need to specify a time range."))
            return

        try:
            keep = timediff_from_str(' '.join(args))
        except InvalidTimeDiffString as e:
            call.reply(_("Error: {0}").format(e))

        d = call.confirm(
            "This will delete the record of all servers registered later than {0}, continue?".format(str_from_timediff(keep))
            )
        def _confirmed(ret):
            if ret:
                def done(*args):
                    call.reply(_("Done."))
                self.servers._purge(keep).addCallback(done)
            else:
                call.reply(_("Cancelled."))
        d.addCallback(_confirmed)

    def listServers(self, call, args):
        """!listservers

        Lists all servers that are registered with the bot's GameServer interface.
        """
        servers = self.servers.servers
        #print servers

        if len(servers) == 0:
            call.reply(_("No servers registered yet."))
            return

        def do_call(is_op):
            keys = sorted(servers.keys())
            if is_op:
                reply = config.get("GameServer Interface", "server list").decode('string-escape')%\
                        { 'servers': ", ".join(["{0} ({1})".format(k, servers[k].index) for k in keys]),
                          'num_servers': len(servers), }
            else:
                reply = config.get("GameServer Interface", "server list").decode('string-escape')%\
                        { 'servers': ", ".join(["{0}".format(k) for k in keys]),
                          'num_servers': len(servers), }
            call.reply(reply, ', ')
        FetchedList.has_flag(self.pypickupbot, self.pypickupbot.channel, call.nick, 'o').addCallback(do_call)

    def listActiveServers(self, call, args):
        """!activeservers

        Lists all servers that are registered with the bot's GameServer interface and currently have players.
        THIS COMMAND CAN TAKE SOME TIME TO RUN THROUGH!
        """
        if len(self.servers.servers) == 0:
            call.reply(_("No servers registered yet."))
            return

        if not self._check_timer():
            call.reply(_("Please wait..."))
            return

        servers = []
        servernames = sorted(self.servers.servers.keys())
        for name in servernames:
            server = self.servers.servers[name]
            if server.is_available() and server.num_players() > 0:
                servers.append(server)

        reply = config.get("GameServer Interface", "server list active").decode('string-escape')%\
                { 'servers': ", ".join([
                    config.get("GameServer Interface", "server list servers").decode('string-escape')%\
                    {
                      'name':  s.get_name(),
                      'details': "{0}/{1}".format(s.num_players(), s.max_players())
                    }
                    for s in servers]),
                  'num_servers': len(servers), }
        call.reply(reply, ', ')

    def listAvailableServers(self, call, args):
        """!servers

        Lists all servers that are registered with the bot's GameServer interface with a short status indicator.
        THIS COMMAND CAN TAKE SOME TIME TO RUN THROUGH!
        """
        if len(self.servers.servers) == 0:
            call.reply(_("No servers registered yet."))
            return

        if not self._check_timer():
            call.reply(_("Please wait..."))
            return

        servers = []
        servernames = sorted(self.servers.servers.keys())
        for name in servernames:
            server = self.servers.servers[name]
            if server.is_available():
                if server.free_slots() == 0:
                    item = config.get("GameServer Interface", "server quicklist full").decode('string-escape')%\
                           { 'name': name, }
                elif server.num_players() == 0:
                    item = config.get("GameServer Interface", "server quicklist empty").decode('string-escape')%\
                           { 'name': name, }
                else:
                    item = config.get("GameServer Interface", "server quicklist free").decode('string-escape')%\
                           { 'name': name, }
            else:
                item = config.get("GameServer Interface", "server quicklist down").decode('string-escape')%\
                       { 'name': name, }
            servers.append(item)

        reply = config.get("GameServer Interface", "server quicklist").decode('string-escape')%\
                { 'servers': ", ".join(servers),
                  'num_servers': len(servers), }
        call.reply(reply, ', ')

    def listEmptyServers(self, call, args):
        """!emptyservers

        Lists all servers that are registered with the bot's GameServer interface and currently have no players.
        THIS COMMAND CAN TAKE SOME TIME TO RUN THROUGH!
        """
        if len(self.servers.servers) == 0:
            call.reply(_("No servers registered yet."))
            return

        if not self._check_timer():
            call.reply(_("Please wait..."))
            return

        servers = []
        servernames = sorted(self.servers.servers.keys())
        for name in servernames:
            server = self.servers.servers[name]
            if server.is_available() and server.num_players() == 0:
                servers.append(server)

        reply = config.get("GameServer Interface", "server list empty").decode('string-escape')%\
                { 'servers': ", ".join([
                    config.get("GameServer Interface", "server list servers").decode('string-escape')%\
                    {
                      'name':       s.get_name(),
                      'details':    "{0} slots".format(s.max_players())
                    }
                    for s in servers]),
                  'num_servers': len(servers), }
        call.reply(reply, ', ')

    def findServer(self, call, args):
        """!findserver <text>

        Finds all servers that are registered with the bot's GameServer interface and whose names contain the given text. Ops will get the database indices in addition to the registered names (e.g. to remove servers).
        """
        if not len(args) == 1:
            call.reply(_("You need to specify a text to search for."))
            return

        servers = self.servers.servers
        if len(servers) == 0:
            call.reply(_("No servers registered yet."))
            return

        if not self._check_timer():
            call.reply(_("Please wait..."))
            return

        servers = self.servers._search(args[0])
        if len(servers) == 0:
            call.reply(_("No servers found."))
            return

        def do_call(is_op):
            keys = sorted(servers.keys())
            if is_op:
                reply = config.get("GameServer Interface", "server search").decode('string-escape')%\
                { 'servers': ", ".join([ "({1}) {0}".format(k, servers[k].index) for k in keys ]),
                  'num_servers': len(servers), }
            else:
                reply = config.get("GameServer Interface", "server search").decode('string-escape')%\
                { 'servers': ", ".join([ "{0}".format(k) for k in keys ]),
                  'num_servers': len(servers), }
            call.reply(reply, ', ')
        FetchedList.has_flag(self.pypickupbot, self.pypickupbot.channel, call.nick, 'o').addCallback(do_call)

    def serverStatus(self, call, args):
        """!server <name>

        Shows some information about the given server (e.g. number of players, current game ...)."""
        if not len(args) >= 1:
            call.reply(_("You must specify the server (short-name)."))
            return

        server = self.servers._find_server(args[0])
        if not server:
            call.reply(_("This server is not registered!"))
            return

        if not self._check_timer():
            call.reply(_("Please wait..."))
            return

        status = ""
        details = ""
        if server.is_available():
            if server.is_full():
                status = config.get("GameServer Interface", "server status full").decode('string-escape')
            elif server.is_empty():
                status = config.get("GameServer Interface", "server status empty").decode('string-escape')
            else:
                status = config.get("GameServer Interface", "server status free").decode('string-escape')

            details = config.get("GameServer Interface", "server status details").decode('string-escape')%\
                { 'name': server.get_name(),
                  'ip': server.get_ip(),
                  'fullname': server.get_fullname(),
                  'ping': server.get_ping(),
                  'num_players': server.num_players(),
                  'max_players': server.max_players(),
                  'num_spectators': server.num_spectators(),
                  'location': server.get_location(),
                  'current_map': server.get_map(),
                  'version': server.get_gameversion(),
                  'mod': server.get_gamemod(),
                  'gametype': server.get_gametype(),
                }
        else:
            status = config.get("GameServer Interface", "server status down").decode('string-escape')

        reply = config.get("GameServer Interface", "server status").decode('string-escape')%\
            { 'name': server.get_name(),
              'ip': server.get_ip(),
              'status': status,
              'details': details,
            }

        if call.channel == 'PM':
            call.reply(reply)
        else:
            self.pypickupbot.cmsg(reply, call.channel)

    def serverCurrentPlayers(self, call, args):
        """!playerstatus <name>

        Shows the players currently connected to a server (playing and spectating)."""
        if not len(args) >= 1:
            call.reply(_("You must specify the server (short-name)."))
            return

        if not self._check_timer():
            call.reply(_("Please wait..."))

        server = self.servers._find_server(args[0])
        if not server:
            call.reply(_("This server is not registered!"))
            return

        if not server.is_available():
            call.reply(_("This server is currently not available."))
            return

        players = server.get_players()
        spectators = server.get_spectators()
        if len(players) > 0:
            if len(spectators) == 0:
                spectators.append("N/A")
            reply = config.get("GameServer Interface", "server current players").decode('string-escape')%\
                { 'name': server.get_name(),
                  'ip': server.get_ip(),
                  'num_players': server.num_players(),
                  'max_players': server.max_players(),
                  'num_spectators': server.num_spectators(),
                  'players': ', '.join([
                        config.get('GameServer Interface', 'server current players list').decode('string-escape')%\
                        {
                            'nick': irc_colors(qfont_decode(nick)),
                        }
                        for nick in players]),
                  'spectators': ', '.join([
                        config.get('GameServer Interface', 'server current players list').decode('string-escape')%\
                        {
                            'nick': irc_colors(qfont_decode(nick)),
                        }
                        for nick in spectators]),
                }
        elif len(spectators) > 0:
            reply = config.get("GameServer Interface", "server current players onlyspec").decode('string-escape')%\
                { 'name': server.get_name(),
                  'ip': server.get_ip(),
                  'num_players': server.num_players(),
                  'max_players': server.max_players(),
                  'num_spectators': server.num_spectators(),
                  'spectators': ', '.join([
                        config.get('GameServer Interface', 'server current players list').decode('string-escape')%\
                        {
                            'nick': irc_colors(qfont_decode(nick)),
                        }
                        for nick in spectators]),
                }
        else:
            reply = config.get("GameServer Interface", "server current players empty").decode('string-escape')%\
                { 'name': server.get_name(),
                  'ip': server.get_ip(),
                  'num_players': server.num_players(),
                  'max_players': server.max_players(),
                  'num_spectators': server.num_spectators(),
                }

        call.reply(reply)

    def serverIP(self, call, args):
        """!serverip <name>

        Shows the IP address for the given server."""
        if not len(args) >= 1:
            call.reply(_("You must specify the server (short-name)."))
            return

        server = self.servers._find_server(args[0])
        if not server:
            call.reply(_("This server is not registered!"))
            return

        reply = config.get("GameServer Interface", "server ip").decode('string-escape')% \
            { 'name':   server.get_name(),
              'ip':     server.get_ip(),
            }

        if call.channel == 'PM':
            call.reply(reply)
        else:
            self.pypickupbot.cmsg(reply, call.channel)

    def announce(self, call, args):
        """!announce <name> [channel]

        Announce server in given channel (or default list if unspecified) to make players join."""
        if not len(args) >= 1:
            call.reply(_("You must specify the server (short-name)."))
            return

        server = self.servers._find_server(args[0])
        if not server:
            call.reply(_("This server is not registered!"))
            return

        announce_channels = config.get("GameServer Interface", "announce channels").decode('string-escape').split()
        if not self.pypickupbot.channel in announce_channels:
            announce_channels.insert(0, self.pypickupbot.channel)  # add home channel to front

        channels = []
        if len(args) >= 2:
            channels = args[1:]
            if sum([ (0 if c in announce_channels else 1) for c in channels ]) > 0:
                call.reply(_("Sorry, you can only specify channels defined in the config!"))
                return
        else:
            channels = announce_channels  # use default list

        admin = self.pypickupbot.is_admin_or_voiced(call.user, call.nick)
        def _knowAdmin(admin):
            if self.last_announce + config.getint('GameServer Interface', 'announce delay') > time() \
                    and not admin:
                call.reply(_("Can't announce so often."))
                return

            def _confirmed(ret):
                if ret:
                    self.last_announce = time()
                    if server.num_players() > 0:
                        message = config.get("GameServer Interface", "announce message").decode('string-escape')
                    else:
                        message = config.get("GameServer Interface", "announce message empty").decode('string-escape')
                    cmsg = message % \
                        {
                            'name':     server.get_name(),
                            'ip':       server.get_ip(),
                            'fullname': server.get_fullname(),
                            'location': server.get_location(),      # GeoIP location
                            'players':  server.num_players(),       # num players
                            'map':      server.get_map(),           # current map name
                            'version':  server.get_gameversion(),   # game version
                            'mod':      server.get_gamemod(),       # game mod: XPM, ...
                            'gametype': server.get_gametype(),      # gametype: dm, tdm, ctf, ...
                        }
                    for chan in channels:
                        self.pypickupbot.cmsg(cmsg, channel=chan)
                        sleep(1)
                else:
                    call.reply(_("Cancelled."))
            if server.num_players() > 0:
                d = defer.Deferred()
                return d.addCallback(_confirmed).callback(True)
            else:  # ask for confirmation if server is empty
                d = call.confirm(_("Server <{0}> (\x02{1}\x02) is empty, do you really want to publically announce it?").\
                        format(server.get_name(), server.get_fullname()))
                return d.addCallback(_confirmed)
        admin.addCallbacks(_knowAdmin)

    def addServer(self, call, args):
        """!addserver <name> <ip>

        Registers a new server which will be used for pickup games. If successful, a public message will be shown on the channel. This command is only available to admins.
        """

        if not len(args) == 2:
            call.reply(_("You must specify the short-name and the server ip."))
            return

        server = self.servers._find_server(args[0])
        if server:
            call.reply(_("This server is already registered as {0} - can't continue! ").\
                    format(server.get_name()))
            return

        servername = args[0]
        serverip   = args[1]

        if re.match("^(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)(:\d{1,5})$", serverip) == None:  # ... now that's a fancy regex ;)
            call.reply(_("This IP address doesn't look valid - can't continue! "))
            return

        server = Server(serverip, servername)
        if not server.is_valid():
            call.reply(_("This server doesn't seem to be accessible!"))
            return

        d = call.confirm(_("You're about to add a new server <{1}> with IP {0} (\x02{2}\x02), " + \
                "is this correct?").\
                format(serverip, servername, server.get_fullname()))
        def _confirmed(ret):
            if ret:
                def done(*args):
                    def done(*args):
                        server = self.servers._find_server(servername)
                        call.reply(_("Done."))
                        msg = config.get('GameServer Interface', 'server registered').decode('string-escape')%\
                            {   'ip':       serverip,
                                'name':     servername,
                                'fullname': server.get_fullname(),
                                'location': server.get_location(),
                            }
                        self.pypickupbot.msg(self.pypickupbot.channel, msg)
                    self.servers._load_from_db().addCallback(done)
                self.servers._insert(serverip, servername).addCallback(done)
            else:
                call.reply(_("Cancelled."))
        d.addCallback(_confirmed)

    def removeServer(self, call, args):
        """!removeserver <name|index>

        Removes a specific server from the bot's GameServer database.

        Using the index allows to delete a specific item from the database. Ops can use !listservers to find out the index.
        """
        if not len(args) == 1:
            call.reply(_("You need to specify one server name."))
            return

        name = args[0]
        d = call.confirm(_("This will delete all entries registered to {0}, continue?").format(name))
        def _confirmed(ret):
            if ret:
                def done(*args):
                   def done(*args):
                        call.reply(_("Done."))
                   d = self.servers._load_from_db()
                   d.addCallback(done)
                return self.servers._delete(name).addCallback(done)
            else:
                call.reply(_("Cancelled."))
        return d.addCallback(_confirmed)

    def findPlayer(self, call, args):
        """!findplayer <text>

        Searches all registered server for players matching the given search string."""
        _all_colors = re.compile(r'\^(\d|x[\dA-Fa-f]{3})')

        if len(args) == 0:
            call.reply(_("You must specify the text to search for!"))
            return

        if not self._check_timer():
            call.reply(_("Please wait..."))
            return

        servers = self.servers.servers
        result = []
        for k,s in servers.items():
            for p in s.get_players():
                nick = _all_colors.sub('', qfont_decode(p.lower()))
                found = True
                for text in args:
                    if not text.lower() in nick:
                        found = False
                        break
                if found:
                    result.append( (p, k) )

        if len(result) == 0:
            call.reply(_("No player '{0}' found on any registered server.").format(" ".join(args)))
            return

        reply = config.get("GameServer Interface", "server player search").decode('string-escape')%\
            { 'search': ' '.join(args),
              'count': len(result),
              'players': ', '.join([
                    config.get('GameServer Interface', 'server player search list').decode('string-escape')%\
                    {
                        'nick': irc_colors(qfont_decode(nick)),
                        'server': server,
                    }
                    for nick,server in result]),
            }
        call.reply(reply, ', ')

    def pickServer(self, call, args):
        """!pickserver [slots] [mod] [location]

        Choose an empty(!) server which is available for playing right now. You can specify a minimum number of player slots, a mod to look for (Xonotic, MinstaGib, Overkill, XPM), and a location (e.g. DE, NL) to narrow down the search. It is also possible to combine multiple mods or locations by separating them with commas.
        """

        slots = 0
        mods = []
        locations = []

        if len(args) > 0:
            try:
                slots = int(args[0])
            except:
                call.reply(_("Number of slots must be an integer - cant' continue!"))
                return

        if len(args) > 1:
            mods = [s.lower() for s in args[1].split(",")]

        if len(args) > 2:
            locations = [s.lower() for s in args[2].split(",")]

        if not self._check_timer():
            call.reply(_("Please wait..."))
            return

        slist = []
        servers = self.servers.servers
        for k,s in servers.items():
            if s.is_available() and s.is_empty():
                if s.max_players() >= slots:
                    if len(mods) == 0 or s.get_gamemod().lower() in mods:
                        if len(locations) == 0 or s.get_location().lower() in locations:
                            slist.append(s)

        if len(slist) == 0:
            call.reply(_("Could't find any server matching the given criteria!"))
            return

        reply = config.get("GameServer Interface", "server pick result").decode('string-escape')%\
            {
              'count': len(slist),
              'servers': ', '.join([
                config.get("GameServer Interface", "server pick result list").decode('string-escape')%\
                {
                  'name': server.get_name(),
                  'ip': server.get_ip(),
                  'max_players': server.max_players(),
                  'location': server.get_location(),
                }
                for server in slist]),
            }
        call.reply(reply, ', ')

    def autopickServer(self, game, players, captains):
        servers = self.servers.servers
        if len(servers) == 0:
            return

        if config.get("GameServer Interface", "autopick"):
            servers = {}
            for name,server in self.servers.servers.items():
                #if server.is_available() and server.num_players() == 0 and server.max_players() >= len(players) and server.is_private():
                if server.is_available() and server.is_empty() and server.is_private():
                        servers[name] = server

            # TODO - show more useful server list

            keys = sorted(servers.keys())
            msg = config.get("GameServer Interface", "server list autopick").decode('string-escape')%\
                    { 'servers': ", ".join([
                        config.get("GameServer Interface", "server list servers").decode('string-escape')%\
                        {
                          'name':  k,
                          'details': "{0} slots".format(servers[k].max_players())
                        }
                        for k in keys]),
                      'num_servers': len(servers),
                      'prefix': config.get('Bot', 'command prefix'),
                    }
            self.pypickupbot.cmsg(msg)
        else:
            servers = self.servers.servers
            keys = sorted(servers.keys())
            msg = config.get("GameServer Interface", "server shortlist").decode('string-escape')%\
                    { 'servers': ", ".join([
                        config.get("GameServer Interface", "server shortlist servers").decode('string-escape')%\
                        {
                          'name':  k
                        }
                        for k in keys]),
                      'num_servers': len(servers),
                      'prefix': config.get('Bot', 'command prefix'),
                    }
            self.pypickupbot.cmsg(msg)

    def __init__(self, bot):
        """Plugin init

        Reads servers from config"""
        self.servers = GameServerInterface()
        self.pypickupbot = bot
        self.lastcmd = 0
        self.last_announce = 0

    commands = {
        'listservers':      (listServers,           0),
        'activeservers':    (listActiveServers,     0),
        'emptyservers':     (listEmptyServers,      0),
        'servers':          (listAvailableServers,  0),
        'findserver':       (findServer,            COMMAND.NOT_FROM_OUTSIDE),
        'pickserver':       (pickServer,            COMMAND.NOT_FROM_OUTSIDE),
        'server':           (serverStatus,          0),
        'serverip':         (serverIP,              0),
        'playerstatus':     (serverCurrentPlayers,  0),
        'findplayer':       (findPlayer,            0),
        'announce':         (announce,              COMMAND.NOT_FROM_OUTSIDE),

        'addserver':        (addServer,     COMMAND.NOT_FROM_OUTSIDE | COMMAND.ADMIN),
        'removeserver':     (removeServer,  COMMAND.NOT_FROM_OUTSIDE | COMMAND.ADMIN),
        'clearservers':     (clearServers,  COMMAND.NOT_FROM_OUTSIDE | COMMAND.ADMIN),
        'purgeservers':     (purgeServers,  COMMAND.NOT_FROM_OUTSIDE | COMMAND.ADMIN),
    }

    eventhandlers = {
        'pickup_game_started':  autopickServer,
    }

game_server = SimpleModuleFactory(PickupGameServerBot)

