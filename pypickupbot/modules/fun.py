# pypickupbot - An ircbot that helps game players to play organized games
#               with captain-picked teams.
#     Copyright (C) 2010 pypickupbot authors
#     Copyright (C) 2012-2017 Jan Behrens <zykure42@gmail.com>
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.

"""Pickup-related commands"""
from time import time, sleep
from datetime import datetime
import random
import json
import urllib
import string

from twisted.python import log
from twisted.internet import defer

from pypickupbot.modable import SimpleModuleFactory
from pypickupbot.irc import COMMAND, InputError, FetchedList
from pypickupbot.topic import Topic
from pypickupbot import db
from pypickupbot import config
from pypickupbot.misc import str_from_timediff, timediff_from_str,\
    InvalidTimeDiffString, StringTypes, itime
from pypickupbot.pygeoip import Database


class FunBot:

    # anti-spam feature:
    #  - single users can't use spam commands within 10 seconds
    #  - all users can't use spam commands within 10 seconds
    #  - if users try to spam within this time, their timeout is increased by 10 seconds per try
    USER_SPAM_TIMEOUT           = 15
    USER_SPAM_TIMEOUT_INC       = 15
    GLOBAL_SPAM_TIMEOUT         = 5

    # 1% chance to get the special reply to !spam
    SPAM_SPECIAL_REPLY_CHANCE   = 0.02

    # max. number of reply components to !spam (minimum is 2)
    SPAM_MAX_REPLY_COUNT        = 12

    # 7% chance of accidentally comitting suicide on !kill
    KILL_SUICIDE_CHANCE         = 0.07


    users     = {}  # user renames

    lastspam  = 0   # global timestamp of last spam command
    spammers  = {}  # users' timestamps of last spam command
    spamcount = {}  # users' spam command count

    def _get_nick(self, nick):
        if self.users.has_key(nick):
            return self.users[nick]
        else:
            return nick

    def _check_spam(self, nick):
        now = time()

        nick = self._get_nick(nick)

        if self.spamcount.has_key(nick):
            self.spamcount[nick] += 1
        else:
            self.spamcount[nick] = 0

        # prevent global spamming
        if abs(now - self.lastspam) < self.GLOBAL_SPAM_TIMEOUT:
            return False

        # prevent single users from spamming
        if self.spammers.has_key(nick):
            if abs(now - self.spammers[nick]) < self.USER_SPAM_TIMEOUT + self.USER_SPAM_TIMEOUT_INC * self.spamcount[nick]:
                return False

        self.spamcount[nick] = 0
        self.spammers[nick]  = now
        self.lastspam        = now
        return True

    def spam(self, call, args):
        if random.random() <= self.SPAM_SPECIAL_REPLY_CHANCE:
            call.reply("Lobster Thermidor aux crevettes with a Mornay sauce, garnished with truffle pate, brandy and a fried egg on top and Spam")
        else:
            _replies = [
                "Spam","Spam","Spam",  # 3x higher probability for 'Spam'
                "Eggs",
                "Bacon",
                "Sausage",
                "Baked beans",
            ]
            count = random.randint(2, self.SPAM_MAX_REPLY_COUNT)
            reply = ", ".join([random.choice(_replies) for x in range(count-1)]) + " and " + random.choice(_replies)
            call.reply(reply)

        return

    def hello(self, call, args):
        _replies = [
            "Hello {0}, how are you today?",
            "Hey {0}!",
            "Hi {0}, 'sup?",
            "Greetings, {0}!",
        ]
        call.reply( random.choice(_replies).format(call.nick) )

    def die(self, call, args):
        _replies = [
            "Not today!",
            "I'll live to see another day!",
            "Oh god, no!!!",
            "Come at me, bro!!",
        ]
        call.reply( random.choice(_replies) )

    def what(self, call, args):
        _replies = [
            "Are you stupid?",
            "How would I know?!",
            "I don't get it.",
            "Who?",
        ]
        call.reply( random.choice(_replies) )

    def why(self, call, args):
        _replies = [
            "I don't know!",
            "How should I know that?!",
            "What do you think?",
            "What?",
        ]
        call.reply( random.choice(_replies) )

    def kill(self, call, args):
        # Death and suicide messages from Xonotic:
        # https://gitlab.com/xonotic/xonotic-data.pk3dir/raw/master/qcsrc/common/notifications/all.inc

        if len(args) != 1:
            call.reply(_("Do you want to die or something?!"))
            return

        if not self._check_spam(call.nick):
            call.reply(_("Don't spam ({0})!").format(self.spamcount[call.nick]))
            return

        _suicides = [
            # Accordeon
            "{0} hurt their own ears with the @!#%'n Accordeon",
            # Blaster
            "{0} shot themself to hell with their Blaster",
            # Crylink
            "{0} felt the strong pull of their Crylink",
            # Devastator
            "{0} blew themself up with their Devastator",
            # Electro
            "{0} played with Electro bolts",
            "{0} could not remember where they put their Electro orb",
            # Fireball
            "{0} should have used a smaller gun",
            "{0} forgot about their firemine",
            # Hagar
            "{0} played with tiny Hagar rockets",
            # HLAC
            "{0} got a little jumpy with their HLAC",
            # Klein bottle
            "{0} hurt their own ears with the @!#%%'n Klein Bottle",
            # Minelayer
            "{0} forgot about their mine",
            # Mortar
            "{0} didn't see their own Mortar grenade",
            "{0} blew themself up with their own Mortar",
            # RPC
            "{0} was sawn in half by their own Rocket Propelled Chainsaw",
            "{0} blew themself up with their Rocket Propelled Chainsaw",
            # Seeker
            "{0} played with tiny Seeker rockets",
            # Tuba
            "{0} hurt their own ears with the @!#%%'n Tuba",
            # Other suicides
            "{0} became enemies with the Lord of Teamplay",
            "{0} thought they found a nice camping ground",
            "{0} unfairly eliminated themself",
            "{0} couldn't catch their breath",
            "{0} was in the water for too long",
            "{0} hit the ground with a crunch",
            "{0} hit the ground with a bit too much force",
            "{0} became a bit too crispy",
            "{0} felt a little hot",
            "{0} died",
            "{0} turned into hot slag",
            "{0} found a hot place",
            "{0} was exploded by a Mage",
            "{0} 's innards became outwards by a Shambler",
            "{0} was smashed by a Shambler",
            "{0} was zapped to death by a Shambler",
            "{0} was bitten by a Spider",
            "{0} was fireballed by a Wyvern",
            "{0} joins the Zombies",
            "{0} was given kung fu lessons by a Zombie",
            "{0} mastered the art of self-nading",
            "{0} was burned to death by their own Napalm Nade",
            "{0} decided to take a look at the results of their napalm explosion",
            "{0} was frozen to death by their own Ice Nade",
            "{0} felt a little chilly",
            "{0} 's Healing Nade didn't quite heal them",
            "{0} died. What's the point of living without ammo?",
            "{0} ran out of ammo",
            "{0} rotted away",
            "{0} became a shooting star",
            "{0} was slimed",
            "{0} couldn't take it anymore",
            "{0} is now preserved for centuries to come",
            "{0} died in an accident",
            "{0} ran into a turret",
            "Reconsider your tactics, {0}",
            "{0} was unfairly eliminated",
            "{0} couldn't catch their breath",
            "{0} hit the ground with a crunch",
            "{0} got a little bit too crispy",
            "{0} felt a little too hot",
            "{0} killed their own dumb self",
            "{0} needs to be more careful",
            "{0} couldn't stand the heat",
            "{0} was killed by a monster",
            "{0} needs to watch out for monsters",
            "{0} forgot to put the pin back in",
            "{0}, hanging around a napalm explosion is bad",
            "{0} got a little bit too cold",
            "{0} felt a little chilly",
            "{0}'s Healing Nade is a bit defective",
            "{0} was killed for running out of ammo",
            "{0} grew too old without taking their medicine",
            "{0} needs to preserve their health",
            "{0} became a shooting star",
            "{0} melted away in slime",
            "{0} committed suicide",
            "{0} ended it all",
            "{0} got stuck in a swamp",
            "{0} died in an accident",
            "{0} was fragged by a turret",
            "{0} had an unfortunate run in with a turret",
            "{0} was crushed by a vehicle",
            "{0}, watch your step",
        ]

        _kills = [
            # Accordeon
            "{1} died of {0}'s great playing on the @!#%%'n Accordeon",
            # Arc
            "{1} was electrocuted by {0}'s Arc",
            "{1} was blasted by {0}'s Arc bolts",
            # Blaster
            "{1} was shot to death by {0}'s Blaster",
            # Crylink
            "{1} felt the strong pull of {0}'s Crylink",
            # Devastator
            "{1} ate {0}'s rocket",
            "{1} got too close {0}'s rocket",
            # Electro
            "{1} was blasted by {0}'s Electro bolt",
            "{1} felt the electrifying air of {0}'s Electro combo",
            "{1} got too close to {0}'s Electro orb",
            # Fireball
            "{1} got too close to {0}'s fireball",
            "{1} got burnt by {0}'s firemine",
            # Hagar
            "{1} was pummeled by a burst of {0}'s Hagar rockets",
            "{1} was pummeled by {0}'s Hagar rockets",
            # HLAC
            "{1} was cut down with {0}'s HLAC",
            # HMG
            "{1} was sniped by {0}'s Heavy Machine Gun",
            "{1} was torn to bits by {0}'s Heavy Machine Gun",
            # Hook
            "{1} was caught in {0}'s Hook gravity bomb",
            # Klein bottle
            "{1} died of {0}'s great playing on the @!#%%'n Klein Bottle",
            # Machine gun
            "{1} was sniped by {0}'s Machine Gun",
            "{1} was riddled full of holes by {0}'s Machine Gun",
            # Minelayer
            "{1} got too close to {0}'s mine",
            # Mortar
            "{1} got too close to {0}'s Mortar grenade",
            "{1} ate {0}'s Mortar grenade",
            # Rifle
            "{1} was sniped with a Rifle by {0}",
            "{1} died in {0}'s Rifle bullet hail",
            "{1} failed to hide from {0}'s Rifle bullet hail",
            "{1} failed to hide from {0}'s Rifle",
            # RPC
            "{1} was sawn in half by {0}'s Rocket Propelled Chainsaw",
            "{1} almost dodged {0}'s Rocket Propelled Chainsaw",
            # Seeker
            "{1} was pummeled by {0}'s Seeker rockets",
            "{1} was tagged by {0}'s Seeker",
            # Shockwave
            "{1} was gunned down by {0}'s Shockwave",
            "{1} slapped {0} around a bit with a large Shockwave",
            # Shotgun
            "{1} was gunned down by {0}'s Shotgun",
            "{1} slapped {0} around a bit with a large Shotgun",
            # Tuba
            "{1} died of {0}'s great playing on the @!#%%'n Tuba",
            # Vaporizer
            "{1} has been sublimated by {0}'s Vaporizer",
            # Vortex
            "{1} has been vaporized by {0}'s Vortex",
            # Other kills
            "{1} was unfairly eliminated by {0}",
            "{1} was drowned by {0}",
            "{1} was grounded by {0}",
            "{1} was burnt up into a crisp by {0}",
            "{1} felt a little hot from {0}'s fire",
            "{1} was cooked by {0}",
            "{1} was pushed infront of a monster by {0}",
            "{1} was blown up by {0}'s Nade",
            "{1} was burned to death by {0}'s Napalm Nade",
            #"{1} got too close to a napalm explosion",
            "{1} was blown up by {0}'s Ice Nade",
            "{1} was frozen to death by {0}'s Ice Nade",
            "{1} has not been healed by {0}'s Healing Nade",
            "{1} was shot into space by {0}",
            "{1} was slimed by {0}",
            "{1} was preserved by {0}",
            "{1} was telefragged by {0}",
            "{1} tried to occupy {0}'s teleport destination space",
            "{1} died in an accident with {0}",
            "{1} was crushed by {0}",
            "{1} was thrown into a world of hurt by {0}",
            "{1} was betrayed by {0}",
            #"{1} tried to occupy {0}'s teleport destination space",
            #"{1} was telefragged by {0}",
            #"{1} was drowned by {0}",
            #"{1} was slimed by {0}",
            #"{1} was cooked by {0}",
            #"{1} was grounded by {0}",
            #"{1} was shot into space by {0}",
            #"{1} was conserved by {0}",
            #"{1} was thrown into a world of hurt by {0}",
            #"{1} was crushed by {0}",
            "{1} got shredded by {0}",
            "{1} was blasted to bits by {0}",
            "{1} was bolted down by {0}",
            "{1} could find no shelter from {0}'s rockets",
            "{1} was nailed to hell by {0}",
            "{1} was pushed into the line of fire by {0}",
            "{1} was pushed into an accident by {0}",
            "{1} was unfairly eliminated by {0}",
            "{1} was burnt to death by {0}",
            "{1} was fragged by {0}",
        ]

        killed = args[0]
        killer = call.nick

        users = []
        d = FetchedList.get_users(self.pypickupbot, self.pypickupbot.channel).get()
        def _fireEvent(userlist):
            users = []
            for nick, ident, host, flags in userlist:
                users.append(nick)
            if (killer == killed) or (not killed in users) or (killed == self.pypickupbot.nickname) \
                    or (random.random() <= self.KILL_SUICIDE_CHANCE):
                reply = random.choice(_suicides).format(killer)
            else:
                reply = random.choice(_kills).format(killer, killed)
            if not reply[-1] in ".!?":
                reply += '.'
            self.pypickupbot.cmsg(reply.encode('utf-8'), call.channel)
        d.addCallback(_fireEvent)


    def rot13(self, call, args):
        _rot13 = string.maketrans(
            "ABCDEFGHIJKLMabcdefghijklmNOPQRSTUVWXYZnopqrstuvwxyz",
            "NOPQRSTUVWXYZnopqrstuvwxyzABCDEFGHIJKLMabcdefghijklm")
        result = string.translate(" ".join(args), _rot13)
        call.reply(result)

    def say(self, call, args):
        if not self._check_spam(call.nick):
            call.reply(_("Don't spam ({0})!").format(self.spamcount[call.nick]))
            return

        if len(args) < 5:
            call.reply(_("That's just lame!"))
            return

        random.shuffle(args)
        reply = " ".join(args)
        if call.channel == 'PM':
            call.reply(reply)
        else:
            self.pypickupbot.cmsg()
        self.pypickupbot.cmsg(reply.encode('utf-8'), call.channel)

    def google(self, call, args):
        if not self._check_spam(call.nick):
            call.reply(_("Don't spam ({0})!").format(self.spamcount[call.nick]))
            return

        if len(args) == 0:
            call.reply(_("What are you looking for?"))
            return

        query = urllib.urlencode({'q': " ".join(args)})
        url = 'http://ajax.googleapis.com/ajax/services/search/web?v=1.0&%s' % query
        search_response = urllib.urlopen(url)
        search_results = search_response.read()
        results = json.loads(search_results)
        hits = results['responseData']['results']

        if len(hits) == 0:
            call.reply(_("Can't find what you're looking for!"))
            return

        reply = _("Let me google that for you: {0}").format(hits[0]['url'])
        self.pypickupbot.cmsg(reply.encode('utf-8'), call.channel)

    def dice(self, call, args):
        if not self._check_spam(call.nick):
            call.reply(_("Don't spam ({0})!").format(self.spamcount[call.nick]))
            return

        sides = 6  # default dice has 6 sides
        if len(args) > 0:
            try:
                sides = int(args[0])
            except:
                call.reply(_("Argument must be an integer!"))
                return

        result = random.randint(1, min(4,sides))  # smallest dice has 4 sides (a tetraeder)
        reply = _("Rolling a {1}-sided dice gives: {0}").format(result, sides)
        if call.channel == 'PM':
            call.reply(reply)
        else:
            self.pypickupbot.cmsg(reply.encode('utf-8'), call.channel)

    def cointoss(self, call, args):
        if not self._check_spam(call.nick):
            call.reply(_("Don't spam ({0})!").format(self.spamcount[call.nick]))
            return

        result = random.choice(["Heads", "Tails"])
        reply = _("Cointoss gives: {0}".format(result))
        if call.channel == 'PM':
            call.reply(reply)
        else:
            self.pypickupbot.cmsg(reply.encode('utf-8'), call.channel)

    def userRenamed(self, oldname, newname):
        """track user renames"""
        if self.users.has_key(oldname):
            self.users[newname] = self.users[oldname]
            del self.users[oldname]
        else:
            self.users[newname] = oldname

    def __init__(self, bot):
        """Plugin init"""
        self.pypickupbot = bot

        log.msg("Fun mode engaged! :o)")

    commands = {
        'spam':             (spam,      0),
        'hello':            (hello,     0),
            'hi':           (hello,     0),
            'hey':          (hello,     0),
        'die':              (die,       0),
        'what':             (what,      0),
        'why':              (why,       0),
        'rot13':            (rot13,     0),

        'kill':             (kill,      COMMAND.NOT_FROM_PM),
        'say':              (say,       COMMAND.NOT_FROM_PM),
        'google':           (google,    COMMAND.NOT_FROM_PM),
        'dice':             (dice,      COMMAND.NOT_FROM_PM),
        'cointoss':         (cointoss,  COMMAND.NOT_FROM_PM),
    }

    eventhandlers = {
        'userRenamed':          userRenamed,
    }

fun = SimpleModuleFactory(FunBot)

