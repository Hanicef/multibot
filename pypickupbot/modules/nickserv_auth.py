# pypickupbot - An ircbot that helps game players to play organized games
#               with captain-picked teams.
#     Copyright (C) 2010 pypickupbot authors
#     Copyright (C) 2012-2017 Jan Behrens <zykure42@gmail.com>
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.

"""Authenticates with NickServ"""

from twisted.python import log
from twisted.internet import defer, reactor

from pypickupbot.modable import SimpleModuleFactory
from pypickupbot import config

class NickServAuth:
    def __init__(self, bot):
        """Plugin init"""
        self.pypickupbot = bot
        self.deferred    = None

    def signedOn(self):
        self.deferred = defer.Deferred()

        user = config.get('NickServ Auth', 'username')
        password = config.get('NickServ Auth', 'password')

        self.pypickupbot.sendLine("PRIVMSG NickServ :IDENTIFY %s %s" % (user,password))
        log.msg("Trying to authenticate as {0}...".format(user))

        self.isAuthed = False
        def _replied(msg):
            if self.isAuthed:
                log.msg('Logged in as %s.' % config.get('NickServ Auth', 'username'))
                # try again to join previously blocked channels
                self.pypickupbot.joinChannels()
            else:
                log.msg('Authentication failed.')

        self.deferred.addCallback(_replied)
        reactor.callLater(10, _replied, None)  # timeout

    def noticed(self, user, target, message):
        if self.deferred == None or self.deferred.called:
            return

        if target == self.pypickupbot.nickname \
                and user == "NickServ!NickServ@services.":
            if message == "You are now identified for \02%s\x02." %\
                    config.get('NickServ Auth', 'username'):
                self.isAuthed = True
                self.deferred.callback(message)
            elif message == "Invalid password for \x02%s\x02." %\
                    config.get('NickServ Auth', 'username'):
                self.isAuthed = False
                self.deferred.callback(message)
            else:
                log.err("Unknown message from NickServ: %s" % message)
                self.deferred.cancel()

    eventhandlers = {
        'signedOn':     signedOn,
        'noticed':      noticed,
        }

nickserv_auth = SimpleModuleFactory(NickServAuth)
