# -*- coding: utf-8 -*-
# pypickupbot - An ircbot that helps game players to play organized games
#               with captain-picked teams.
#     Copyright (C) 2010 pypickupbot authors
#     Copyright (C) 2012-2017 Jan Behrens <zykure42@gmail.com>
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.

"""Pickup-related commands"""
from time import time, sleep, localtime, asctime
from datetime import datetime
from collections import namedtuple
from math import floor, ceil
import random
import json
import httplib
import re
import thread
import glob
import sys, os.path
import unicodedata

from twisted.python import log
from twisted.internet import defer

from pypickupbot.modable import SimpleModuleFactory
from pypickupbot.irc import COMMAND, InputError, FetchedList
from pypickupbot.topic import Topic
from pypickupbot import db
from pypickupbot import config
from pypickupbot.misc import str_from_timediff, timediff_from_str,\
    InvalidTimeDiffString, StringTypes, itime

_game_regex = re.compile('(([0-9]+)v([0-9]+))?([a-zA-Z]+).*')

# Map of special chars to ascii from Darkplace's console.c.
_qfont_table = [
 '\0', '#',  '#',  '#',  '#',  '.',  '#',  '#',
 '#',  '\t', '\n', '#',  ' ',  '\r', '.',  '.',
 '[',  ']',  '0',  '1',  '2',  '3',  '4',  '5',
 '6',  '7',  '8',  '9',  '.',  '<',  '=',  '>',
 ' ',  '!',  '"',  '#',  '$',  '%',  '&',  '\'',
 '(',  ')',  '*',  '+',  ',',  '-',  '.',  '/',
 '0',  '1',  '2',  '3',  '4',  '5',  '6',  '7',
 '8',  '9',  ':',  ';',  '<',  '=',  '>',  '?',
 '@',  'A',  'B',  'C',  'D',  'E',  'F',  'G',
 'H',  'I',  'J',  'K',  'L',  'M',  'N',  'O',
 'P',  'Q',  'R',  'S',  'T',  'U',  'V',  'W',
 'X',  'Y',  'Z',  '[',  '\\', ']',  '^',  '_',
 '`',  'a',  'b',  'c',  'd',  'e',  'f',  'g',
 'h',  'i',  'j',  'k',  'l',  'm',  'n',  'o',
 'p',  'q',  'r',  's',  't',  'u',  'v',  'w',
 'x',  'y',  'z',  '{',  '|',  '}',  '~',  '<',

 '<',  '=',  '>',  '#',  '#',  '.',  '#',  '#',
 '#',  '#',  ' ',  '#',  ' ',  '>',  '.',  '.',
 '[',  ']',  '0',  '1',  '2',  '3',  '4',  '5',
 '6',  '7',  '8',  '9',  '.',  '<',  '=',  '>',
 ' ',  '!',  '"',  '#',  '$',  '%',  '&',  '\'',
 '(',  ')',  '*',  '+',  ',',  '-',  '.',  '/',
 '0',  '1',  '2',  '3',  '4',  '5',  '6',  '7',
 '8',  '9',  ':',  ';',  '<',  '=',  '>',  '?',
 '@',  'A',  'B',  'C',  'D',  'E',  'F',  'G',
 'H',  'I',  'J',  'K',  'L',  'M',  'N',  'O',
 'P',  'Q',  'R',  'S',  'T',  'U',  'V',  'W',
 'X',  'Y',  'Z',  '[',  '\\', ']',  '^',  '_',
 '`',  'a',  'b',  'c',  'd',  'e',  'f',  'g',
 'h',  'i',  'j',  'k',  'l',  'm',  'n',  'o',
 'p',  'q',  'r',  's',  't',  'u',  'v',  'w',
 'x',  'y',  'z',  '{',  '|',  '}',  '~',  '<'
]

def qfont_decode(qstr=''):
    """ Convert the qfont characters in a string to ascii.
    """
    if qstr == None:
        qstr = ''
    chars = []
    for c in qstr:
        if u'\ue000' <= c <= u'\ue0ff':
            c = _qfont_table[ord(c) - 0xe000]
        chars.append(c)
    s = u''.join(chars).replace('\x00','')
    #return unicodedata.normalize("NFKD", s).encode('utf-8', errors='replace')
    #return s.encode('ascii', errors='replace')
    return unicodedata.normalize("NFKD", s).encode('ascii', errors='replace')

PlayerTimeout = namedtuple('PlayerTimeout', ['added', 'messaged'])


#######################################################################################################################

class Player:

    def __init__(self, nick, playerid = None, subscriptions = None, create_dt = None, skill = -1, index = None):
        self.nick           = nick
        self.playerid       = playerid
        self.subscriptions  = [] if not subscriptions else subscriptions.split(":")
        self.create_dt      = create_dt
        self.skill          = skill
        self.index          = index
        self.player_info    = None

    def __str__(self):
        return "{0} (#{1})".format(self.nick, self.playerid)

    def get_xonstat_url(self):
        """return xontat url for specific player"""
        return config.get("Xonstat Interface", "url").decode('string-escape') + "player/" + str(self.playerid)

    def get_id(self):
        return self.playerid

    def _get_xonstat_json(self, request):
        server = config.get("Xonstat Interface", "server").decode('string-escape')
        data = ""
        try:
            http = httplib.HTTPConnection(server)
            http.connect()
            http.request("GET", request)
            response = http.getresponse()
            data = response.read()
            http.close()
        except:
            return {}
        json_data = json.loads(data)
        return json_data[0]  # dict embedded in a list

    def _get_player_info(self):
        if not self.player_info:
            self.player_info = self._get_xonstat_json("/player/{0}.json".format(self.playerid))
        return self.player_info

    def is_valid(self):
        self._get_player_info()
        if not self.player_info:
            return False
        if len(self.player_info) == 0:
            return False
        return True

    def _irc_colors(self, qstr, bold=False):
        _irc_colors = [ -1, 4, 9, 8, 12, 11, 13, -1, -1, -1 ]
        def _rgb_to_simple(r,g,b):
            # this was basically taken from rcon2irc.pl
            min_ = min(r,g,b)
            max_ = max(r,g,b)

            v = max_ / 15.0
            s = (1 - min_/max_) if max_ != min_ else 0

            if s < 0.2:
                return 0 if v < 0.5 else 7

            if max_ == min_:
                h = 0
            elif max_ == r:
                h = (60 * (g - b) / (max_ - min_)) % 360
            elif max_ == g:
                h = (60 * (b - r) / (max_ - min_)) + 120
            elif max_ == b:
                h = (60 * (r - g) / (max_ - min_)) + 240

            if h < 36:
                return 1
            elif h < 80:
                return 3
            elif h < 150:
                return 2
            elif h < 200:
                return 5
            elif h < 270:
                return 4
            elif h < 330:
                return 6
            else:
                return 1

        _all_colors = re.compile(r'(\^\d|\^x[\dA-Fa-f]{3})')
        #qstr = ''.join([ x if ord(x) < 128 else '' for x in qstr ]).replace('^^', '^').replace(u'\x00', '') # strip weird characters
        parts = _all_colors.split(qstr)
        result = "\002"
        oldcolor = None
        while len(parts) > 0:
            tag = None
            txt = parts[0]
            if _all_colors.match(txt):
                tag = txt[1:]  # strip leading '^'
                if len(parts) < 2:
                    break
                txt = parts[1]
                del parts[1]
            del parts[0]

            if not txt or len(txt) == 0:
                # only colorcode and no real text, skip this
                continue

            color = 7
            if tag:
                if len(tag) == 4 and tag[0] == 'x':
                    r = int(tag[1], 16)
                    g = int(tag[2], 16)
                    b = int(tag[3], 16)
                    color = _rgb_to_simple(r,g,b)
                elif len(tag) == 1 and int(tag[0]) in range(0,10):
                    color = int(tag[0])
            color = _irc_colors[color]
            if color != oldcolor:
                if color < 0:
                    result += "\017\002"
                else:
                    result += "\003" + "%02d" % color
            result += txt
            oldcolor = color
        result += "\017"

        return result

    def get_nick(self, gametype = None):
        try:
            nick = qfont_decode( self._get_player_info()['player']['nick'] )
        except:
            nick = self.nick
        nick = self._irc_colors(nick, bold=True)
        if gametype != None and self.get_elo(gametype) == None:
            nick += config.get("Pickup messages", "player noelo-mark").decode('string-escape')
        return nick

    def has_skill(self):
        return (self.skill > 0)  # only True if skill is valid!

    def get_skill(self):
        return self.skill

    def get_stripped_nick(self):
        return qfont_decode( self._get_player_info()['player']['stripped_nick'] )

    def get_elo_dict(self):
        return self._get_player_info()['elos']

    def get_rank_dict(self):
        return self._get_player_info()['ranks']

    def has_elo(self, gametype):
        return (self.get_elo(gametype) != None)

    def get_elo(self, gametype):
        gt = self._get_gametype(gametype)
        try:
            elos = self._get_player_info()['elos']
            if elos.has_key(gt):
                return elos[gt]['elo']
        except:
            pass
        return None

    def has_rank(self, gametype):
        return (self.get_rank(gametype)[0] != None)

    def get_rank(self, gametype):
        gt = self._get_gametype(gametype)
        try:
            ranks = self._get_player_info()['ranks']
            if ranks.has_key(gt):
                return (ranks[gt]['rank'], ranks[gt]['max_rank'])
        except:
            pass
        return (None, None)

    def _get_gametype(self, gamenick):
        for target,games in config.items('Xonstat Games'):
            games = [ x.strip() for x in games.decode('string-escape').split(",") ]
            if gamenick in games:
                return target
        return None

    def add_subscription(self, game):
        if not game in self.subscriptions:
            self.subscriptions.append(game)

    def remove_subscription(self, game):
        if game in self.subscriptions:
            self.subscriptions.remove(game)

#######################################################################################################################

class Team:

    def __init__(self, name, gametype):
        self.players    = []
        self.name       = name
        self.gametype   = ""
        self.elo        = 0
        self.skill      = 0
        self.captain    = None
        self.eloplayercount = 0
        self.skillplayercount = 0

        for target,games in config.items('Xonstat Games'):
            games = [ x.strip() for x in games.decode('string-escape').split(",") ]
            if gametype in games:
                self.gametype = target
                break

    def is_valid(self):
        return (self.gametype != None)

    def __str__(self):
        return self.name + ": " + ", ".join([ str(p) for p in self.players])

    def get_players(self):
        return self.players

    def add_player(self, player):
        self.players.append(player)
        elo = player.get_elo(self.gametype)
        if elo:
            self.elo += elo
            self.eloplayercount += 1
        skill = player.get_skill()
        if player.has_skill():
            self.skill += skill
            self.skillplayercount += 1

    def remove_player(self, player):
        for p in self.players:
            if p == player:
                elo = player.get_elo()
                if elo:
                    self.elo -= elo
                    self.eloplayercount -= 1
                skill = player.get_skill()
                if player.has_skill():
                    self.skill -= skill
                    self.skillplayercount -= 1
                self.players.remove(p)
                return True
        return False

    def mean_elo(self):
        if self.eloplayercount > 0:
            return float(self.elo) / self.eloplayercount
        return 0  # default value

    def mean_skill(self):
        if self.skillplayercount > 0:
            return self.skill / self.skillplayercount
        return 7  # default value

    def playercount(self):
        return (len(self.players), len(self.players) - self.eloplayercount, len(self.players) - self.skillplayercount )

    def auto_add_player(self, other, pickpool):
        elo_diff = self.mean_elo() - other.mean_elo()
        elo = None
        player = None
        for p in pickpool:
            p_elo = p.get_elo(self.gametype)
            if not p_elo:
                p_elo = 100.  # default value
            if not elo or (elo_diff >  0 and p_elo < elo) or (elo_diff <= 0 and p_elo > elo):
                elo = p_elo
                player = p

        log.msg("{0}: auto-adding {1} ({2} elo)".format(self.name, player.nick, player.get_elo(self.gametype)))
        self.add_player(player)
        return player

#######################################################################################################################

class Game:
    """A game that can be played in the channel"""
    def __init__(self, pickup, nick, name, captains=2, players=8, autopick=True, **kwargs):
        self.pickup = pickup
        self.xonstat = pickup.xonstat
        self.nick = nick
        self.name = name
        self.caps = int(captains)
        self.maxplayers = int(players)
        if type(autopick) == str:
            if autopick.lower() == "no":
                self.autopick = False
            elif autopick.lower() == "yes":
                self.autopick = True
        else:
            self.autopick = bool(autopick)
        self.info = kwargs
        self.players = []
        self.starting = False
        self.abortstart = False

        if 'teamnames' in kwargs:
            self.teamnames = config.getlist('Pickup: '+nick, 'teamnames')
        else:
            self.teamnames = []

    def pre_start(self):
        """Initiates a game's start"""
        self.starting = True
        start = self.pickup.pypickupbot.fire('pickup_game_pre_start', self)

        def _knowStart(start):
            if not start:
                self.pickup.pypickupbot.cmsg(
                    _("%s game about to start..")%self.name
                )
            else:
                self.do_start()
        start.addCallback(_knowStart)

    def abort_start(self):
        """Aborts a starting game"""
        if self.starting:
            self.abortstart = True
        return self

    def abort(self):
        """Aborts a starting game"""
        if self.starting:
            self.abortstart = True
        if len(self.players):
            self.players = []
            self.pickup.update_topic()
        self.starting = False
        return self

    def _get_teamnames(self, num):
        teamnames = []
        if config.getboolean('Pickup', 'random teamnames'):
            wordlists = self.pickup.wordlists
            if 'adjectives' in wordlists and 'animals' in wordlists:
                adjectives = wordlists['adjectives']
                animals    = wordlists['animals']
                if len(adjectives) >= num and len(animals) >= num:
                    existing = []
                    for t in range(num):
                        while True:
                            letter = chr(96+random.randint(1,26))  # a..z
                            if letter in existing:
                                # make sure each letter is only used once
                                continue
                            existing.append(letter)
                            try:
                                prefix = random.choice([ x for x in adjectives if x[0] == letter ])
                                suffix = random.choice([ x for x in animals if x[0] == letter ])
                            except:
                                continue
                            if suffix[-1] == "s":
                                suffix += "es"
                            else:
                                suffix += "s"
                            teamnames.append("Team '{0} {1}'".format(prefix.capitalize(), suffix.capitalize()))
                            break
                    return teamnames
        # fallback to numbered teams
        for t in range(num):
            teamnames.append("Team {0}".format(t+1))
        return teamnames

    def do_start(self):
        """Does the actual starting of the game"""
        if self.abortstart or not self.starting:
            self.abortstart = False
            self.starting = False
            return

        players = self.players[:self.maxplayers]
        for player in players:
            self.pickup.all_games().force_remove(player)

        self.pickup.update_topic()

        self.pickup.pypickupbot.notice(self.pickup.pypickupbot.channel,
            _("%(gamenick)s game ready to start in %(channel)s")
                % {'gamenick': self.nick, 'channel': self.pickup.pypickupbot.channel})

        # flatten player list ([[a,b],[c,d]] -> [a,b,c,d])
        for p in players:  # flatten list
            if type(p) == list:
                players.remove(p)
                players.extend(p)

        # Set up teams
        self.teamnames = self._get_teamnames(self.caps)

        gametype = self.nick
        teams = []
        for name in self.teamnames:
            teams.append(Team(name, gametype))

        captains = []
        cmsg = ""  # public channel message
        pmsg = ""  # player query message

        if not self.autopick or self.caps != 2:  # only 2 teams supported with autopicking
            pickpool = []
            for p in players:
                nick = self.xonstat._get_original_nick(p)
                player = self.xonstat._find_player(nick)
                if not player:
                    log.msg("Player {0} ({1}) is not registered".format(nick, p))
                    player = Player(p, None)
                pickpool.append(player)

            players = sorted(pickpool, key=lambda p: p.playerid)
            captains = random.sample(pickpool, self.caps)

            playerlist  = [ p.nick for p in players ]
            captainlist = [ p.nick for p in captains ]
            self.pickup.pypickupbot.fire('pickup_game_starting', self, playerlist, captainlist)

            log.msg("Players: {0} / Captains: {1}".format(playerlist, captainlist))

            if len(captains) > 0:
                cmsg = config.get('Pickup messages', 'game ready').decode('string-escape')%\
                    {
                        'nick': self.nick,
                        'playernum': len(self.players),
                        'playermax': self.maxplayers,
                        'name': self.name,
                        'numcaps': self.caps,
                        'playerlist': ', '.join([
                                config.get('Pickup messages', 'game ready player elo').decode('string-escape')%
                                {
                                    'nick': p.get_nick(),
                                    'name': p.nick,
                                    'playerid': p.playerid,
                                    'elo': round(p.get_elo(self.nick),1) if p.has_elo(self.nick) else "???",
                                }
                                for p in players]),
                        'captainlist': ', '.join([
                                config.get('Pickup messages', 'game ready captain').decode('string-escape')%
                                {
                                    'nick': p.get_nick(),
                                    'name': p.nick,
                                    'playerid': p.playerid,
                                }
                                for p in captains]),
                    }

                if config.getboolean("Pickup", "PM each player on start"):
                    pmsg = config.get("Pickup messages", "youre needed").decode('string-escape')%\
                        {
                            'channel': self.pickup.pypickupbot.channel,
                            'name': self.name,
                            'nick': self.nick,
                            'numcaps': self.caps,
                            'playerlist': ', '.join([
                                config.get('Pickup messages', 'game ready player').decode('string-escape')%
                                {
                                    'nick': p.get_nick(),
                                    'name': p.nick,
                                    'playerid': p.playerid,
                                }
                                for p in players]),
                            'captainlist': ', '.join([
                            config.get('Pickup messages', 'game ready captain').decode('string-escape')%
                                {
                                    'nick': p.get_nick(),
                                    'name': p.nick,
                                    'playerid': p.playerid,
                                }
                                for p in captains]),
                        }

            else:
                cmsg = config.get('Pickup messages', 'game ready nocaptains').decode('string-escape')%\
                    {
                        'nick': self.nick,
                        'playernum': len(self.players),
                        'playermax': self.maxplayers,
                        'name': self.name,
                        'numcaps': self.caps,
                        'playerlist': ', '.join([
                                config.get('Pickup messages', 'game ready player elo').decode('string-escape')%
                                {
                                    'nick': p.get_nick(),
                                    'name': p.nick,
                                    'playerid': p.playerid,
                                    'elo': round(p.get_elo(self.nick),1) if p.has_elo(self.nick) else "???",
                                }
                                for p in players]),
                    }

                if config.getboolean("Pickup", "PM each player on start"):
                    pmsg  = config.get("Pickup messages", "youre needed nocaptains").decode('string-escape')%\
                        {
                            'channel': self.pickup.pypickupbot.channel,
                            'name': self.name,
                            'nick': self.nick,
                            'numcaps': self.caps,
                            'playerlist': ', '.join([
                                config.get('Pickup messages', 'game ready player').decode('string-escape')%
                                {
                                    'nick': p.get_nick(),
                                    'name': p.nick,
                                    'playerid': p.playerid,
                                }
                                for p in players]),
                        }

        else:  # if not self.autopick
            # Create a pickpool containing Player instances
            pickpool = []
            for p in players:
                nick = self.xonstat._get_original_nick(p)
                player = self.xonstat._find_player(nick)
                if not player:
                    log.msg("Player {0} ({1}) is not registered".format(nick, p))
                    player = Player(p, None)
                pickpool.append(player)

            players = sorted(pickpool, key=lambda p: p.playerid)
            captainpool = pickpool[:]

            # Randomly select one captain, then select other one with similar elo
            # FIXME - only two teams are supported currently - FIXME
            team1, team2 = teams[:2]

            playerpool = []
            for skill in [1,2,3,4,5,6]:
                pool_elo = [ p for p in pickpool if (p.skill == skill and p.has_elo(gametype)) ]
                pool_noelo = [ p for p in pickpool if (p.skill == skill and not p.has_elo(gametype)) ]
                if len(pool_elo) > 0:
                    playerpool.extend( sorted(pool_elo, key=lambda p: -p.get_elo(gametype)) )
                if len(pool_noelo) > 0:
                    random.shuffle(pool_noelo)
                    playerpool.extend(pool_noelo)

            playerpool_unknown = []
            pool_elo = [ p for p in pickpool if (p.get_skill() <= 0 and p.has_elo(gametype)) ]
            pool_noelo = [ p for p in pickpool if (p.get_skill() <= 0 and not p.has_elo(gametype)) ]
            if len(pool_elo) > 0:
                playerpool_unknown.extend( sorted(pool_elo, key=lambda p: -p.get_elo(gametype)) )
            if len(pool_noelo) > 0:
                random.shuffle(pool_noelo)
                playerpool_unknown.extend(pool_noelo)

            # auto-select players (based on skill)
            log.msg("Adding known players: {0}".format(", ".join([ p.nick for p in playerpool ])))
            while len(playerpool) > 0:
                #print str([ p.nick for p in team1.players ]), team1.mean_skill(), team1.mean_elo()
                #print str([ p.nick for p in team2.players ]), team2.mean_skill(), team2.mean_elo()
                if len(team1.players) == 0:
                    p = playerpool[0]  # get best player for start
                elif team1.mean_skill() > team2.mean_skill():
                    # team2 is better / NOTE: skill gets better towards lower numbers
                    p = playerpool[0]  # get best player
                elif team1.mean_skill() < team2.mean_skill():
                    # team1 is better
                    p = playerpool[-1]  # get worst player
                else:
                    if team1.mean_elo() < team2.mean_elo():
                        # team 2 is better / NOTE: elo gets better towards higher numbers
                        p = playerpool[0]
                    elif team1.mean_elo() > team2.mean_elo():
                        p = playerpool[-1]
                    else:
                        p = random.choice(playerpool)
                #p = team1.auto_add_player(team2, playerpool)
                team1.add_player(p)
                playerpool.remove(p)  # also remove from global pool
                team1, team2 = team2, team1  # swap

            # randomly pick remaining players
            log.msg("Adding remaining players: {0}".format(", ".join([ p.nick for p in playerpool_unknown ])))
            while len(playerpool_unknown) > 0:
                if team1.mean_elo() < team2.mean_elo():
                    p = playerpool_unknown[0]
                elif team1.mean_elo() > team2.mean_elo():
                    p = playerpool_unknown[-1]
                else:
                    p = random.choice(playerpool_unknown)
                team1.add_player(p)
                playerpool_unknown.remove(p)
                team1, team2 = team2, team1  # swap


            captain_elo_diff = "???"
            #print captainpool
            if len(captainpool) > 2:
                count = 0
                while count < 5:  # loop up to 5 times until caps have been found
                    count += 1
                    skill = random.randint(1,6)  # randomly pick skill
                    captains1, captains2 = [], []
                    for p in captainpool:
                        if abs(p.skill - skill) <=  1:  # allow +/- 1 skill point diff.
                            if p in team1.players:
                                captains1.append(p)
                            if p in team2.players:
                                captains2.append(p)
                    if len(captains1) > 0 and len(captains2) > 0:
                        break

                # if the above didn't succeed, fall back to old system
                if min(len(captains1), len(captains2)) == 0:
                    log.msg("Could not provide team-based captains, falling back to global elo!")
                    captains1, captains2 = [], []
                    for p in captainpool:
                        if p in team1.players:
                            captains1.append(p)
                        if p in team2.players:
                            captains2.append(p)

                team1.captain = random.choice(captains1)
                best_diff = None
                for p in captains2:
                    elo1 = team1.captain.get_elo(gametype)
                    elo2 = p.get_elo(gametype)
                    if not elo1:
                        elo1 = 100
                    if not elo2:
                        elo2 = 100
                    elo_diff = abs(elo1 - elo2)
                    if not best_diff or elo_diff < best_diff:
                        team2.captain = p
                        best_diff = elo_diff
                if team2.captain:
                    captain_elo_diff = round(abs(best_diff),1)
                else:
                    team2.captain = random.choice(captains2)
                log.msg("Autopick captains: {0}, {1} (elo diff: {2})".format(team1.captain, team2.captain, captain_elo_diff))
            else:
                team1.captain = random.choice(team1.players)
                team2.captain = random.choice(team2.players)
                log.msg("Random captains: {0}, {1}".format(team1.captain, team2.captain))

            captains = [ team1.captain, team2.captain ]


            if len(team1.players) != len(team2.players):
                log.msg("Teams have different sizes: {0} / {1}".format(team1, team2))

            playerlist  = [p.nick for p in players]
            captainlist = [p.nick for p in captains]
            self.pickup.pypickupbot.fire('pickup_game_starting', self, playerlist, captainlist)

            log.msg("Players: {0} / Captains: {1}".format(playerlist, captainlist))

            cmsg = config.get('Pickup messages', 'game ready autopick').decode('string-escape')%\
                {
                    'nick': self.nick,
                    'playernum': len(players),
                    'playermax': self.maxplayers,
                    'name': self.name,
                    'numcaps': self.caps,
                    'teamslist': '\x09\x0f'.join([
                        config.get('Pickup messages', 'game ready autopick team').decode('string-escape')%
                        {
                            'name': t.name,
                            'players': ', '.join([
                                config.get('Pickup messages', 'game ready player').decode('string-escape')%
                                {
                                    'nick': p.get_nick(gametype),
                                    'name': p.nick,
                                    'playerid': p.playerid,
                                }
                                for p in t.players]),
                            'mean_elo': round(t.mean_elo(),1),
                            'elo_prefix': "~" if t.playercount()[1] > 0 else "",
                        }
                        for t in teams]),
                    'captainlist': ', '.join([
                                config.get('Pickup messages', 'game ready captain').decode('string-escape')%
                                {
                                    'nick': p.get_nick(gametype),
                                    'name': p.nick,
                                    'playerid': p.playerid,
                                }
                                for p in captains]),
                    'elo_diff': captain_elo_diff,
                }

            if config.getboolean("Pickup", "PM each player on start"):
                pmsg = config.get("Pickup messages", "youre needed").decode('string-escape')%\
                    {
                        'channel': self.pickup.pypickupbot.channel,
                        'name': self.name,
                        'nick': self.nick,
                        'numcaps': self.caps,
                        'playerlist': ', '.join([
                            config.get('Pickup messages', 'game ready player').decode('string-escape')%
                            {
                                'nick': p.get_nick(gametype),
                                'name': p.nick,
                                'playerid': p.playerid,
                            }
                            for p in players]),
                        'captainlist': ', '.join([
                            config.get('Pickup messages', 'game ready captain').decode('string-escape')%
                            {
                                'nick': p.get_nick(gametype),
                                'name': p.nick,
                                'playerid': p.playerid,
                            }
                            for p in captains]),
                    }

        if len(cmsg) > 0:
            for msg in cmsg.split("\x09"):
                self.pickup.pypickupbot.cmsg(msg)

        self.pickup.pypickupbot.fire('pickup_game_ready', players, teams, pmsg)
        self.pickup.pypickupbot.fire('pickup_game_started', self, playerlist, captainlist)
        self.starting = False

    def teamname(self, i):
        if len(self.teamnames) > i:
            return self.teamnames[i]
        else:
            return _("Team {0}").format(i+1)

    def add(self, call, user, silent=False):
        """Add a player to this game"""
        if user not in self.players:
            self.players.append(user)
            self.pickup.update_topic()
            log.msg("{0} added up for {1}".format(user, self.nick))
            if not silent:
                nick = self.xonstat._get_original_nick(user)
                player = self.xonstat._find_player(nick)
                if not player:
                    msg = config.get("Pickup messages", "youre not registered").decode('string-escape')%\
                    {'prefix':config.get('Bot', 'command prefix')}
                    self.pickup.pypickupbot.pmsg(user, msg)
        if len(self.players) >= self.maxplayers:
            self.pre_start()
            return False

    def who(self):
        """Who is in this game"""
        if len(self.players):
            return config.get('Pickup messages', 'who game').decode('string-escape')%\
                {'nick': self.nick, 'playernum': len(self.players), 'playermax': self.maxplayers,
                 'name': self.name, 'numcaps': self.caps, 'playerlist': ', '.join(self.players) }

    def remove(self, call, user):
        """Removes a player from this game"""
        if user in self.players:
            #print user, self.starting, self.players, len(self.players), self.maxplayers
            if not self.starting or len(self.players) > self.maxplayers:
                self.players.remove(user)
                self.pickup.update_topic()
            else:
                call.reply(_('Too late to remove from %s') % self.nick)

    def force_remove(self, user):
        try:
            self.players.remove(user)
            self.pickup.update_topic()
        except ValueError:
            pass

    def rename(self, oldnick, newnick):
        """Rename a player"""
        try:
            i = self.players.index(oldnick)
            self.players[i] = newnick
        except ValueError:
            pass

#######################################################################################################################

class Games(Game):
    """Groups multiple games to dispatch calls"""
    def __init__(self, games):
        self.games = games

    def remove(self, *args):
        for game in self.games: game.remove(*args)
    def pre_start(self, *args):
        for game in self.games: game.pre_start(*args)
    def force_remove(self, *args):
        for game in self.games: game.force_remove(*args)
    def rename(self, *args):
        for game in self.games: game.rename(*args)

    def add(self, *args):
        for game in self.games:
            if game.add(*args) == False:
                break

    def who(self, *args):
        return [game.who(*args) for game in self.games]

    def abort(self, *args):
        for game in self.games:
            game.abort()
        return self

    def abort_start(self, *args):
        for game in self.games:
            game.abort_start()
        return self

    def __len__(self):
        return len(self.games)

#######################################################################################################################

class XonstatInterface:

    def __init__(self):
        self.players = {}       # nick : playerid

        def _doTransaction(txn):
            txn.execute("""
                CREATE TABLE IF NOT EXISTS
                xonstat_players
                (
                    id          INTEGER PRIMARY KEY AUTOINCREMENT,
                    nick        TEXT,
                    playerid    INTEGER,
                    subscriptions   TEXT,
                    create_dt   INTEGER
                )""")
            txn.execute("""
                CREATE TABLE IF NOT EXISTS
                xonstat_playerskills
                (
                    id          INTEGER PRIMARY KEY AUTOINCREMENT,
                    playerid    INTEGER UNIQUE,
                    skill       INTEGER,
                    create_dt   INTEGER
                )""")
            return txn.fetchall()
        def done(args):
            self._load_from_db()
        db.runInteraction(_doTransaction).addCallback(done)

    def _load_from_db(self):
        #d = db.runQuery("""
        #    SELECT      id, nick, playerid, subscriptions, create_dt
        #    FROM        xonstat_players
        #    ORDER BY    create_dt
        #    """)
        d = db.runQuery("""
            SELECT      xp.id, xp.nick, xp.playerid, xp.subscriptions, xp.create_dt, coalesce(xps.skill, -1)
            FROM        xonstat_players AS xp
            LEFT OUTER JOIN   xonstat_playerskills AS xps
                ON xps.playerid = xp.playerid
            ORDER BY    xp.create_dt
            """)
        self.players = {}
        def _loadPlayers(r):
            for entry in r:
                index, nick, playerid, subscriptions, create_dt, skill = entry
                self.players[nick] = Player(nick, playerid, subscriptions, create_dt, skill, index)
                self.players[nick.lower()] = self.players[nick]  # allow both
        return d.addCallback(_loadPlayers)

    def _find_player(self, nick):
        if nick.lower() in self.players:
            return self.players[nick.lower()]
        return None

    def _find_playerid(self, playerid):
        result = {}
        for k,p in self.players.items():
            if int(p.playerid) == int(playerid):
                result[k] = p
        return result

    def _search(self, string):
        result = {}
        for k,p in self.players.items():
            if string.lower() in p.get_stripped_nick().lower():
                result[k] = p
        return result

    def _purge(self, keep=0):
        """used by clearPlayers and purgePlayers"""
        def _doTransaction(txn):
            txn.execute("""
                DELETE FROM xonstat_players
                WHERE       create_dt < ?
                """, (itime() - keep,))
            txn.execute("""
                DELETE FROM xonstat_playerskills
                WHERE       create_dt < ?
                """, (itime() - keep,))
            return txn.fetchall()
        def onErr(failure):
            log.err(failure, "purge players, keep = {0}".format(keep))
            return failure
        return db.runInteraction(_doTransaction).addErrback(onErr)

    def _delete(self, nick):
        if nick.startswith("#"):
            playerid = int(nick[1:])
            def _doTransaction(txn):
                txn.execute("""
                    DELETE FROM xonstat_playerskills
                    WHERE       playerid  = ?
                    """, (playerid,))
                txn.execute("""
                    DELETE FROM xonstat_players
                    WHERE       playerid = ?
                    """, (playerid,))
                return txn.fetchall()
            return db.runInteraction(_doTransaction)
        else:
            def _doTransaction(txn):
                txn.execute("""
                    DELETE FROM xonstat_playerskills
                    WHERE       playerid IN (
                        SELECT DISTINCT playerid
                        FROM xonstat_players
                        WHERE   nick=?
                    )
                    """, (nick,))
                txn.execute("""
                    DELETE FROM xonstat_players
                    WHERE       nick=?
                    """, (nick,))
                return txn.fetchall();
            return db.runInteraction(_doTransaction)

    def _insert(self, nick, playerid, subscriptions=[]):
        return db.runOperation("""
            INSERT INTO xonstat_players(nick, playerid, subscriptions, create_dt)
            VALUES      (:nick, :playerid, :subscriptions, :ctime)
            """, (nick, playerid, ":".join(subscriptions), itime(),))

    def _update_subscriptions(self, nick, subscriptions=[]):
        return db.runOperation("""
            UPDATE  xonstat_players
            SET     subscriptions= :subscriptions
            WHERE   nick = :nick
            """, (":".join(subscriptions), nick,))

    def _insert_skill(self, playerid, skill):
        return db.runOperation("""
            INSERT INTO xonstat_playerskills(playerid, skill, create_dt)
            VALUES      (:playerid, :skill, :ctime)
            """, (playerid, skill, itime(),))

    def _update_skill(self, playerid, skill):
        return db.runOperation("""
            UPDATE  xonstat_playerskills
            SET     skill = :skill
            WHERE   playerid = :playerid
            """, (skill, playerid,))

    def _get_nick(self, nick):
        return nick

    def _get_original_nick(self, nick):
        return nick

    def _rename_user(self, oldname, newname):
        try:
            self.players[newname] = self.players[oldname]
            self.players[newname.lower()] = self.players[oldname]
            del self.players[oldname]
            del self.players[oldname.lower()]
        except:
            pass

    def _remove_user(self, name):
        try:
            del self.players[name]
            del self.players[name.lower()]
        except:
            pass

#######################################################################################################################

class XonstatPickupBot:
    """Allows the bot to run games with captain-picked teams"""

    def all_games(self):
        """Gets a wrapper for all games"""
        return Games(self.games.values())

    def get_games(self, call, args, implicit_all=True, implicit_active=False):
        """Gets all or some games"""
        if len(args):
            games = []
            for arg in args:
                arg = arg.lower()
                if arg in self.games:
                    games.append(self.games[arg])
                else:
                    call.reply(_("Game %s doesn't exist. Available games are: %s") %\
                            (arg, ', '.join(self.games.keys())))
                    return
#                    m = _game_regex.match(arg)
#                    if not m:
#                        raise InputError(_("Game %s doesn't exist. Available games are: %s") % (arg, ', '.join(self.games.keys())))
#                    else:
#                        p1,p2,gt = m.groups()
#                        if p1 != p2 or gt not in dict(config.items('Pickup Gametypes')):
#                            raise InputError(_("Game %s doesn't exist. Available games are: %s") % (arg, ', '.join(self.games.keys())))
#                        else:
#                            name = "{0}v{1} {2} (auto-added)".format(p1, p2, gt.upper())
#                            if p1 < 1:
#                                raise InputError(_("Game %s doesn't exist. Available games are: %s") % (arg, ', '.join(self.games.keys())))
#                            elif p1 == 1:
#                                g = Game(self, arg, name, captains=0, playerts=2*p1, autopick=True)
#                            else:
#                                g = Game(self, arg, name, captains=2, playerts=2*p1, autopick=True)
#                        log.msg("Auto-adding game: " + arg)
#                        self.games[arg] = g
#                        games.append(g)
            return Games(games)
        elif implicit_active:
            games = []
            for game in self.games.values():
                if len(game.players) > 0:
                    games.append(game)
            return Games(games)
        elif implicit_all:
            return Games(self.games.values())
        else:
            call.reply(_("You need to specify a game."))
            return

    def get_game(self, call, args):
        """Get one game"""
        if len(args) == 1:
            game = args[0]
            if game in self.games:
                return self.games[game]
            else:
                call.reply(_("Game %s doesn't exist. Available games are: %s") % (args[0], ', '.join(self.games.keys())))
                return
        else:
            if len(args) > 1:
                call.reply(_("This command only allows one game to be selected."))
                return
            else:
                call.reply(_("This command needs one game to be selected."))
                return

    def update_topic(self):
        """Update the pickup part of the channel topic"""
        config_topic    = config.getint('Pickup', 'topic')
        config_announce = config.getint('Pickup', 'announce')

        if not (config_topic or config_announce):
            return

        topic, announce = [], []
        for gamenick in self.order:
            game = self.games[gamenick]
            if config_topic == 1 or game.players:
                topic.append(config.get('Pickup messages', 'topic game').decode('string-escape')%\
                    {
                        'nick': game.nick, 'playernum': len(game.players),
                        'playermax': game.maxplayers, 'name': game.name,
                        'numcaps': game.caps
                    })
            if config_announce == 1 or game.players:
                announce.append(config.get('Pickup messages', 'announce game').decode('string-escape')%\
                    {
                        'nick': game.nick, 'playernum': len(game.players),
                        'playermax': game.maxplayers, 'name': game.name,
                        'numcaps': game.caps
                    })

        if config_topic:
            self.topic.update(
                config.get('Pickup messages', 'topic game separator').decode('string-escape')\
                .join(topic)
                )

        if config_announce:
            if len(announce) > 0:
                self.pypickupbot.cmsg(
                    config.get('Pickup messages', 'announce').decode('string-escape') +\
                    config.get('Pickup messages', 'announce game separator').decode('string-escape')\
                    .join(announce)
                    )
            else:
                self.pypickupbot.cmsg(
                    config.get('Pickup messages', 'announce empty').decode('string-escape')
                    )

    def add(self, call, args):
        """!add [game [game ..]]

        Signs you up for one or more games.
        """
        games = self.get_games(call, args,
            config.getboolean("Pickup", "implicit all games in add"),
            config.getboolean("Pickup", "implicit active games in add"),)
        if len(games) > 0:
            games.add(call, call.nick)
            self.pypickupbot.fire("user_added_up", call.nick, games.games)

    def remove(self, call, args):
        """!remove [game [game ..]]

        Removes you from one or more games.
        """
        games = self.get_games(call, args)
        games.remove(call, call.nick)
        self.pypickupbot.fire("user_removed", call.nick, games.games)

    def renew(self, call, args):
        """!renew

        """
        if self.update_timeout(call.nick, False):
            call.reply(_("Thanks for confirming your pickup game(s)."))
        else:
            call.reply(_("You haven't added up for any game(s)!"))

    def who(self, call, args):
        """!who [game [game ..]]

        Shows who has signed up.
    """
        games = [i for i in self.get_games(call, args).who() if i != None]
        all = False
        if len(args) < 1 or len(args) == len(self.games):
            all = True
        if len(games):
            if all:
                call.reply(_("All games:")+" "+config.get('Pickup messages', 'who game separator').decode('string-escape').join(games),
                    config.get('Pickup messages', 'who game separator').decode('string-escape'))
            else:
                call.reply(config.get('Pickup messages', 'who game separator').decode('string-escape').join(games),
                    config.get('Pickup messages', 'who game separator').decode('string-escape'))
        else:
            if all:
                call.reply(_("No game going on!"))
            else:
                call.reply(_("No game in mode %s", "No game in modes %s", len(args)) % ' '.join(args) )

    def promote(self, call, args):
        """!promote <game>

        Shows a notice encouraging players to sign up for the specified game.
        """
        admin = self.pypickupbot.is_admin_or_voiced(call.user, call.nick)
        def _knowAdmin(admin):
            if self.last_promote + config.getint('Pickup', 'promote delay') > time() \
                    and not admin:
                call.reply(_("Can't promote so often."))
                return

            game = self.get_game(call, args)

            if call.nick not in game.players \
                    and not admin:
                call.reply(_("Join the game yourself before promoting it."))
                return

            m = _game_regex.match(game.nick)
            p,p1,p2,gt = None,None,None,"<gametype>"
            if m:
                p,p1,p2,gt = m.groups()

            self.last_promote = time()
            cmsg = config.get('Pickup messages', 'promote').decode('string-escape')%\
                {
                    'bold': '\x02', 'prefix': config.get('Bot', 'command prefix'),
                    'name': game.name, 'nick': game.nick,
                    'gametype': gt,
                    'command': config.get('Bot', 'command prefix')+'add '+game.nick,
                    'channel': self.pypickupbot.channel,
                    'playersneeded': game.maxplayers-len(game.players),
                    'maxplayers': game.maxplayers, 'numplayers': len(game.players),
                }
            self.pypickupbot.cmsg(cmsg)

            log.msg("Checking for highlight on {0} players".format(len(self.xonstat.players)))
            pmsg = config.get('Pickup highlights', 'game promoted').decode("string-escape")%\
                    {
                        'prefix': config.get('Bot', 'command prefix'),
                        'game': game.nick,
                        'channel': self.pypickupbot.channel,
                        'playersneeded': game.maxplayers-len(game.players),
                    }
            players = [ n for n in game.players ]
            for target,player in self.xonstat.players.items():
                if not target in players and len(self._check_highlight(player, [game])) > 0:
                    self.pypickupbot.pmsg(target,
                            "{0}: {1}".format(player.nick, pmsg))
        admin.addCallbacks(_knowAdmin)

    def pull(self, call, args):
        """!pull <player> [game [game ..]]

        Removes someone else from one or more games.
    """
        player = args.pop(0)
        games = self.get_games(call, args).force_remove(player)

    def force_start(self, call, args):
        """!start <game>

        Forces the game to start, even if not enough players signed up"""
        game = self.get_game(call, args)
        if len(game.players) >= game.caps:
            game.pre_start()
        else:
            call.reply(_("Not enough players to choose captains from."))

    def abort(self, call, args):
        """!abort [game [game ..]]

        Aborts a game.
    """
        self.get_games(call, args).abort()

    def pickups(self, call, args):
        """!pickups [search]

        Lists games available for pickups.
        """
        reply = ""
        if len(args) > 0 and args[0].lower() == "all":
            reply +=  ', '.join(
                ["\x0f\x02{0}\x0f ({1})".format(nick, game.name)
                 for nick,game in self.games.iteritems()
                 #if not args or args[0] in nick or args[0] in game.name.lower()
                ])
        else:
            reply += ', '.join(
                ["\x0f\x02{0}\x0f ({1})".format(nick,desc)
                 for nick,desc in config.items('Pickup Gametypes')
                 if not args or args[0] in nick
                ])

        reply += " \x0315(Type e.g. \x02%(prefix)sadd 3v3tdm\x02 to add up for a 3v3 TDM game.)"%\
            {'prefix':config.get('Bot', 'command prefix')}

        call.reply(reply, ', ')

    def clearPlayers(self, call, args):
        """!clearplayers

        Clears the registered players list completely. Prefer the purgeplayers command to this."""
        d = call.confirm("This will delete all registered players, continue?")
        def _confirmed(ret):
            if ret:
                def done(*args):
                    call.reply(_("Done."))
                return self.xonstat._purge().addCallback(done)
            else:
                call.reply(_("Cancelled."))
        return d.addCallback(_confirmed)

    def purgePlayers(self, call, args):
        """!purgeplayers <keep>

        Purges the registered players list from entries created later than [keep] ago."""
        if not len(args) == 1:
            call.reply(_("You need to specify a time range."))
            return

        try:
            keep = timediff_from_str(' '.join(args))
        except InvalidTimeDiffString as e:
            call.reply(_("Error: {}").format(e))
            return

        d = call.confirm(
            "This will delete the record of all players registered later than {0}, continue?".format(str_from_timediff(keep))
            )
        def _confirmed(ret):
            if ret:
                def done(*args):
                    call.reply(_("Done."))
                self.xonstat._purge(keep).addCallback(done)
            else:
                call.reply(_("Cancelled."))
        d.addCallback(_confirmed)

    def whois(self, call, args):
        """!whois <playerid>

        Shows all nicks that have registered to a given playerid. This is useful to find out who has registered to a specific Xonstat account."""
        if not len(args) == 1:
            call.reply(_("You need to specify a playerid to lookup."))
            return

        players = self.xonstat._find_playerid(args[0])
        if len(players) == 0:
            call.reply(_("No players found."))
            return

        def do_call(is_op):
            keys = sorted(players.keys())
            if is_op:
                reply = config.get("Xonstat Interface", "player whois").decode('string-escape')%\
                        { 'players': ", ".join(["{0} ({1})".format(k, players[k].index) for k in keys]),
                          'num_players': len(players), 'gamenick': players.values()[0].get_stripped_nick(), }
            else:
                reply = config.get("Xonstat Interface", "player whois").decode('string-escape')%\
                        { 'players': ", ".join(["{0}".format(k) for k in keys]),
                          'num_players': len(players), 'gamenick': players.values()[0].get_stripped_nick(), }
            call.reply(reply, ', ')
        FetchedList.has_flag(self.pypickupbot, self.pypickupbot.channel, call.nick, 'o').addCallback(do_call)

    def listPlayers(self, call, args):
        """!listplayers

        Lists all players that have registered with the bot's Xonstat interface.
        """
        players = self.xonstat.players
        if len(players) == 0:
            call.reply(_("No players registered yet."))
            return

        def do_call(is_op):
            keys = sorted(players.keys())
            if is_op:
                reply = config.get("Xonstat Interface", "player list").decode('string-escape')%\
                        { 'players': ", ".join(["{0} ({1})".format(k, players[k].index) for k in keys]),
                          'num_players': len(players), }
            else:
                reply = config.get("Xonstat Interface", "player list").decode('string-escape')%\
                        { 'players': ", ".join(["{0}".format(k) for k in keys]),
                          'num_players': len(players), }
            call.reply(reply, ', ')
        FetchedList.has_flag(self.pypickupbot, self.pypickupbot.channel, call.nick, 'o').addCallback(do_call)

    def searchPlayers(self, call, args):
        """!searchplayers <text>

        Finds all players that have registered with the bot's Xonstat interface and whose nicks contain the given text. Ops will get the database indices in addition to the registered names (e.g. to remove players).
        """
        if not len(args) == 1:
            call.reply(_("You need to specify a text to search for."))
            return

        players = self.xonstat._search(args[0])
        if len(players) == 0:
            call.reply(_("No players found."))
            return

        def do_call(is_op):
            keys = sorted(players.keys())
            if is_op:
                reply = config.get("Xonstat Interface", "player search").decode('string-escape')%\
                { 'players': ", ".join([ "({1}) {0}".format(k, players[k].index) for k in keys ]),
                  'num_players': len(players), }
            else:
                reply = config.get("Xonstat Interface", "player search").decode('string-escape')%\
                { 'players': ", ".join([ "{0}".format(k) for k in keys ]),
                  'num_players': len(players), }
            call.reply(reply, ', ')
        FetchedList.has_flag(self.pypickupbot, self.pypickupbot.channel, call.nick, 'o').addCallback(do_call)

    def playerInfo(self, call, args):
        """!info <nick>

        Shows details about a registered player (information from Xonstat). You need to specify the player's IRC nick to look up.
        """
        if not len(args) == 1:
            #call.reply(_("You need to specify a player nickname."))
            #return
            nick = self.xonstat._get_original_nick(call.nick)
        else:
            nick = self.xonstat._get_original_nick(args[0])

        player = self.xonstat._find_player(nick)
        if not player:
            call.reply(_("No player named <{0}> found!").format(nick))
            return

        sep = config.get("Xonstat Interface", "playerinfo separator").decode('string-escape')

        elo_list = []
        for gametype,elo in player.get_elo_dict().items():
            if gametype == 'overall':
                continue
            eloscore, games = round(elo['elo'], 1), elo['games']
            entry = config.get("Xonstat Interface", "playerinfo elo entry").decode('string-escape')%\
                { 'gametype':gametype, 'elo':eloscore, }
            if games < 8:
                # don't show elos with little number of games
                continue
            if games < 32:
                entry += "*"
            elo_list.append(entry)
        elo_list.sort()
        elo_display = sep.join(elo_list)
        if len(elo_list) == 0:
            elo_display = _("none yet")

        rank_list = []
        for gametype,rank in player.get_rank_dict().items():
            if gametype == 'overall':
                continue
            rank, max_rank = rank['rank'], rank['max_rank']
            entry = config.get("Xonstat Interface", "playerinfo rank entry").decode('string-escape')%\
                { 'gametype':gametype, 'rank':rank, 'max_rank':max_rank, }
            rank_list.append(entry)
        rank_list.sort()
        rank_display = sep.join(rank_list)
        if len(rank_list) == 0:
            rank_display = _("none yet")

        reply = config.get("Xonstat Interface", "playerinfo").decode('string-escape')%\
                { 'nick': nick, 'gamenick': player.get_nick(), }
        if len(elo_list) > 0:
            reply += config.get("Xonstat Interface", "playerinfo elos").decode('string-escape')%\
                { 'elos': elo_display }
        if len(rank_list) > 0:
            reply += config.get("Xonstat Interface", "playerinfo ranks").decode('string-escape')%\
                { 'ranks': rank_display, }
        reply += config.get("Xonstat Interface", "playerinfo profile").decode('string-escape')%\
            { 'profile': player.get_xonstat_url(), }
        if player.has_skill():
            reply += config.get("Xonstat Interface", "playerinfo skill").decode('string-escape')%\
                { 'skill': player.get_skill(), }
        call.reply(reply, sep)

    def playerExists(self, call, args):
        """!playerexists <nick>

        Checks if a player has registered with the bot's Xonstat interface.
        """
        if not len(args) == 1:
            call.reply(_("You must name a player to look up."))
            return

        nick = self.xonstat._get_original_nick(args[0])
        player = self.xonstat._find_player(nick)
        if player:
            reply = _("This nick is registered with player id #%(playerid)s (as \x02%(originalnick)s\x02). " + \
                    "Type \x02%(prefix)splayerinfo %(nick)s\x02 to see more details.")%\
                    {'prefix':config.get('Bot', 'command prefix'), 'playerid':player.playerid,
                     'nick':args[0], 'originalnick':nick }
        else:
            reply = _("No player information found for <{0}>.".format(nick))
        call.reply(reply)

    def register(self, call, args):
        """!register <xonstat #id> [nick]

        Registers your nick with the given Xonstat account id. If successful, a public message will be shown on the channel.
        Note that you can register more than one nick to a Xonstat account (e.g. if you use multiple nicks on IRC).
        Admins can specify a nick to force registration for that user.
        """

        # TODO - only allow if user is authed
        # FetchedList.has_flag(self.bot, self.bot.channel, nick, <flag>) == True

        if len(args) < 1:
            call.reply(_("You must specify your Xonstat profile id to register an account."))
            return

        def _knowAdmin(admin, call):
            is_admin = False
            target = call.nick
            if admin:
                is_admin = True
                if len(args) >= 2:  # allow to register other players
                    target = args[1]
            nick = self.xonstat._get_original_nick(target)

            player = self.xonstat._find_player(nick)
            if player:
                if not is_admin:
                    call.reply(_("This nick is already registered with player id #{0} (as <{1}>) - can't continue! " + \
                            "If you need to change your player id, please contact one of the channel operators.").\
                            format(player.get_id(), player.nick))
                    return
                else:
                    call.reply(_("Note: This nick is already registered with player id #{0} (as <{1}>)"). format(player.get_id(), player.nick))

            try:
                playerid = int(args[0])
            except ValueError:
                call.reply(_("Player id must be an integer."))
                return

            player = self.xonstat._find_playerid(playerid)
            if player:
                # SAFETY FEATURE DROPPED
                #raise InputError(_("This player id is already registered to {0} (as <{1}>) - can't continue! " + \
                #        "If you need to change your nick, please contact one of the channel operators.").\
                #        format(player.get_stripped_nick(), player.nick))
                self.pypickupbot.msg(self.pypickupbot.channel,
                        _("This player id is already registered! Please check if your input is correct before you continue. Use !whois {0} to look up who registered to this player id").\
                        format(playerid))

            player = Player(nick, playerid)
            if not player.is_valid():
                call.reply(_("This doesn't seem to be a valid Xonstat playerid!"))
                return

            d = call.confirm(_("You're about to register \x02{3}\x02 with player id #{0} (\x02{1}\x02, " + \
                    "Xonstat profile {2}), is this correct?").\
                    format(player.get_id(), player.get_stripped_nick(), player.get_xonstat_url(), nick))
            def _confirmed(ret):
                if ret:
                    def done(*args):
                        def done(*args):
                            player = self.xonstat._find_player(nick)
                            call.reply(_("Done."))
                            msg = config.get('Xonstat Interface', 'player registered').decode('string-escape')%\
                                { 'nick': target, 'playerid': playerid, 'gamenick': player.get_nick(), 'profile': player.get_xonstat_url(), }
                            self.pypickupbot.msg(self.pypickupbot.channel, msg)
                        self.xonstat._load_from_db().addCallback(done)
                    self.xonstat._insert(nick, playerid).addCallback(done)
                else:
                    call.reply(_("Cancelled."))
            d.addCallback(_confirmed)
        self.pypickupbot.is_admin_or_voiced(call.user, call.nick).addCallback(_knowAdmin, call)


    def removePlayer(self, call, args):
        """!removeplayer <nick|index>

        Removes a specific player from the bot's Xonstat database.

        Using the index allows to delete a specific item from the database. Ops can use !listplayers to find out the index.
        """
        if not len(args) == 1:
            call.reply(_("You need to specify one player name."))
            return

        nick = args[0]
        d = call.confirm(_("This will delete all entries registered to {0}, continue?").format(nick))
        def _confirmed(ret):
            if ret:
                def done(*args):
                   def done(*args):
                        call.reply(_("Done."))
                   d = self.xonstat._load_from_db()
                   d.addCallback(done)
                return self.xonstat._delete(nick).addCallback(done)
            else:
                call.reply(_("Cancelled."))
        return d.addCallback(_confirmed)

    def setSkill(self, call, args):
        """!setskill <nick> [skill]

        Set skill level for given player to be used for teampicking. Skill levels are integers in the range 1..6, with 6 being worst.
        """
        if not len(args) >= 1:
            call.reply(_("You need to specify a player name!"))
            return

        nick = args[0]

        skill = 0  # 0 -> default value
        if len(args) >= 2:
            try:
                skill = int(args[1])
            except:
                pass

            if skill < 1 or skill > 6:
                call.reply(_("The skill setting has to be a number in range 1..6!"))
                return

        player = self.xonstat._find_player(nick)
        if not player:
            call.reply(_("This player does not exist!"))
            return

        if player.get_skill() < 0:  # -1 means it's not in the DB yet
            def done(*args):
                def done(*args):
                    call.reply(_("Done."))
                d = self.xonstat._load_from_db()
                d.addCallback(done)
            self.xonstat._insert_skill(player.get_id(), skill).addCallback(done)
        else:
            if player.get_skill() == skill:
                call.reply(_("Player {0} already has a skill setting of {1}!").format(nick, player.get_skill()))
            else:
                d = call.confirm(_("Player {0} already has a skill setting of {1}. Continue and change setting to {2}?").format(nick, player.get_skill(), skill))
                def _confirmed(ret):
                    if ret:
                        def done(*args):
                            def done(*args):
                                call.reply(_("Done."))
                            d = self.xonstat._load_from_db()
                            d.addCallback(done)
                        self.xonstat._update_skill(player.get_id(), skill).addCallback(done)
                    else:
                        call.reply(_("Cancelled."))
                d.addCallback(_confirmed)

    def subscribe(self, call, args):
        """!subscribe [game [game [...]]]

        Subscribe for highlights on a specific gametype, e.g. ctf or dm.
        """

        if len(args) < 1:
            args = dict(config.items('Pickup Gametypes')).keys()

        player = self.xonstat._find_player(call.nick)
        if not player:
            call.reply(_("You have to register your player id before using this feature! \x0315(Please type \x02%(prefix)sregister <id>\x02 to register.)") %\
                { 'prefix': config.get('Bot', 'command prefix') })
            return

        for arg in args:
            if arg in dict(config.items('Pickup Gametypes')):
                player.add_subscription(arg)

        def done(*args):
            if len(player.subscriptions) > 0:
                call.reply(_("You have subscribed for these gametypes: {0}").format(", ".join(player.subscriptions)))
            else:
                call.reply(_("You have not subscribed for any gametype!"))
        self.xonstat._update_subscriptions(call.nick, player.subscriptions).addCallback(done)

    def unsubscribe(self, call, args):
        """!unsubscribe [game [game [...]]]

        Unsubscribe for highlights on a specific gametype, e.g. ctf or dm.
        """

        if len(args) < 1:
            args = dict(config.items('Pickup Gametypes')).keys()

        player = self.xonstat._find_player(call.nick)
        if not player:
            call.reply(_("You have to register your player id before using this feature! \x0315(Please type \x02%(prefix)sregister <id>\x02 to register.)")%\
                { 'prefix': config.get('Bot', 'command prefix') })
            return

        for arg in args:
            if arg in dict(config.items('Pickup Gametypes')):
                player.remove_subscription(arg)

        def done(*args):
            if len(player.subscriptions) > 0:
                call.reply(_("You have subscribed for these gametypes: {0}").format(", ".join(player.subscriptions)))
            else:
                call.reply(_("You have not subscribed for any gametype!"))
        self.xonstat._update_subscriptions(call.nick, player.subscriptions).addCallback(done)

    def makematch(self, call, args):
        """!makematch <gametype>

        Starts a new match preparation. Use !checkin to add players, !matchlist to retrieve the seed list."""
        channel = config.get("Matchmaking", "home channel")
        if call.channel != channel:
            call.reply(_("You can only use this command in the home channel ({0}).").format(channel))
            return

        gametype = ""
        if len(args) >= 1:
            gametype = args[0]
            #print config.items('Xonstat Games')
            if not gametype in dict(config.items('Xonstat Games')).keys():
                call.reply(_("Gametype {0} is not known.").format(gametype))
                return

        def _confirmed(ret):
            if ret:
                players = self.matchplayers.keys()
                self.pypickupbot.sendLine("MODE %s -%s %s" % \
                        ( channel, "v" * len(players), " ".join(players) ))  # remove +v
                self.matchplayers = {}
                self.matchtype = gametype
                call.reply(_("Done."))
            else:
                call.reply(_("Cancelled."))
        if len(self.matchplayers) > 0:
            d = call.confirm(_("A \x02{0}\x02 match is already in preparation! Continue and clear {1} players?").format(self.matchtype.upper(), len(self.matchplayers)))
            d.addCallback(_confirmed)
        else:
            _confirmed(True)

    def checkin(self, call, args):
        """!checkin <nick1> [nick2 [...]]

        Add players to matchmaking playerlist. This command will add a +v flag to the given users in IRC."""
        channel = config.get("Matchmaking", "home channel")
        if call.channel != channel:
            call.reply(_("You can only use this command in the home channel ({0}).").format(channel))
            return

        if self.matchtype == "":
            call.reply(_("You need to start the matchmaking process via !makematch command."))
            return

        if len(args) < 1:
            call.reply(_("You need to name one or more players (nicknames)."))
            return

        unregistered = []
        unknown = []
        added = []
        for nick in args:
            if nick in self.matchplayers:
                continue
            originalnick = self.xonstat._get_original_nick(nick)
            player = self.xonstat._find_player(originalnick)
            if not player:
                unregistered.append(originalnick)
                continue
            if not player.has_skill():
                unknown.append(nick)
                continue
            self.matchplayers[nick] = player
            added.append(nick)

        pool = added[:]
        while len(pool) > 0:
            currentpool = pool[:6]
            log.msg("Adding +v to {0}".format(" ".join(currentpool)))
            self.pypickupbot.sendLine("MODE %s +%s %s" % \
                    ( channel, "v" * len(currentpool), " ".join(currentpool) ))  # add +v
            for nick in currentpool:
                pool.remove(nick)

        msg = _("Added \x02{0}\x02 new players to \x02{1}\x02 match ({2} total).").format(len(added), self.matchtype, len(self.matchplayers))
        if len(unregistered) > 0:
            msg += _(" Skipped unregistered players: \x02{0}\x02 (use !register <playerid> <nick>).").\
                    format(", ".join(unregistered))
        if len(unknown) > 0:
            msg += _(" Skipped unknown players: \x02{0}\x02 (use !setskill <nick> <skill>).").\
                    format(", ".join(unknown))
        call.reply(msg)


    def checkout(self, call, args):
        """!checkout <nick1> [nick2 [...]]]

        Remove players from matchmaking playerlist. This command will remove a +v flag from the given users in IRC."""
        channel = config.get("Matchmaking", "home channel")
        if call.channel != channel:
            call.reply(_("You can only use this command in the home channel ({0}).").format(channel))
            return

        if self.matchtype == "":
            call.reply(_("You need to start the matchmaking process via !makematch command."))
            return

        if len(args) < 1:
            call.reply(_("You need to name one or more players (nicknames)."))
            return

        removed = []
        for nick in args:
            if not nick in self.matchplayers:
                continue
            del self.matchplayers[nick]
            removed.append(nick)

        pool = removed[:]
        while len(pool) > 0:
            currentpool = pool[:6]
            log.msg("Removing +v from {0}".format(" ".join(currentpool)))
            self.pypickupbot.sendLine("MODE %s -%s %s" % \
                    ( channel, "v" * len(currentpool), " ".join(currentpool) ))  # add +v
            for nick in currentpool:
                pool.remove(nick)

        msg = _("{0} - thanks for playing {1}! =)").format(" ".join(sorted(removed)), self.matchtype.upper())
        self.pypickupbot.cmsg(msg, channel)

        msg = _("Removed \x02{0}\x02 players from \x02{1}\x02 match ({2} total).").format(len(removed), self.matchtype, len(self.matchplayers))
        call.reply(msg)

    def matchlist(self, call, args):
        """!matchlist

        Retrieve ordered list of players in the prepared match. Player are ordered by skill and gametype elo."""
        channel = config.get("Matchmaking", "home channel")
        if call.channel != channel:
            call.reply(_("You can only use this command in the home channel ({0}).").format(channel))
            return

        if self.matchtype == "":
            call.reply(_("You need to start the matchmaking process via !makematch command."))
            return

        players = []
        for skill in [1,2,3,4,5,6,0]:
            pool = [ p for p in self.matchplayers.values() if p.get_skill() == skill ]
            pool = sorted(pool, key=lambda p: -p.get_elo(self.matchtype) if p.has_elo(self.matchtype) else 100.)
            players.extend(pool)

        reply = "Matchlist for \x02%(game)s\x02 game (%(numplayers)d players): %(players)s" % \
            {
                'game':         self.matchtype.upper(),
                'numplayers':   len(players),
                'players':      ", ".join([ "\x02{0}\x02 ({1}/{2})".\
                                format(p.nick, p.get_skill(), round(p.get_elo(self.matchtype),1)) for p in players ]),
            }
        call.reply(reply)

        msg = "\x0313Matchlist for \x02%(game)s\x02 game \x0315(%(numplayers)d players)\x0313:\x0f %(players)s" % \
            {
                'game':         self.matchtype.upper(),
                'numplayers':   len(players),
                'players':      ", ".join([ p.nick for p in players ]),
            }
        self.pypickupbot.cmsg(msg, channel)

    def __init__(self, bot):
        """Plugin init

        Reads games from config"""
        self.xonstat = XonstatInterface()
        self.pypickupbot = bot
        self.game_starting = False

        self.games = {}
        self.order = []
        self.last_promote = 0
        if not config.has_section('Pickup games'):
            log.err('Could not find section "Pickup games" of the config!')
            return
        games = config.items('Pickup games')
#        log.msg(games)

        # read in wordlists
        self.wordlists = {}
        for filename in glob.glob(os.path.dirname(sys.argv[0]) + "/*.lst"):
            name = os.path.splitext(os.path.basename(filename))[0]
            self.wordlists[name] = []
            with open(filename, 'r') as f:
                for line in f.readlines():
                    line = line.strip().lower()
                    if len(line) > 0:
                        self.wordlists[name].append(line)
                log.msg("Read {0} words from wordlist: {1}".format(len(self.wordlists[name]), filename))

        self.matchplayers = {}
        self.matchtype = ""

        for (gamenick, gamename) in games:
            if gamenick == 'order':
                self.order = config.getlist('Pickup games', 'order')
            else:
                if config.has_section('Pickup: '+gamenick):
                    gamesettings = dict(config.items('Pickup: '+gamenick))
                else:
                    gamesettings = {}
                self.games[gamenick] = Game(
                    self,
                    gamenick, gamename,
                    **gamesettings)
                if gamenick not in self.order:
                    self.order.append(gamenick)

        self.order = filter(lambda x: x in self.games, self.order)

        self.player_timeouts = {}
        self.player_highlights = {}
        self._check_timeout()

        self.timeout_thread = thread.start_new_thread(self._timeout_loop, ())

        def _doTransaction(txn):
            txn.execute("""
                CREATE TABLE IF NOT EXISTS
                pickup_games_preparation
                (
                    id      INTEGER PRIMARY KEY AUTOINCREMENT,
                    game    TEXT,
                    player  TEXT,
                    time    INTEGER
                )""")
            txn.execute("""
                    SELECT game,player,time
                    FROM   pickup_games_preparation
                    """)
            return txn.fetchall()
        def done(rows):
            def _fireEvent(userlist):
                users = []
                for nick, ident, host, flags in userlist:
                    users.append(nick)

                added, skipped = [], []
                for game,player,create_dt in rows:
                    if game in self.games:
                        if player in users:
                            if not player in added:
                                added.append(player)
                            log.msg("Adding {1} to {0} game from previous game preparation".\
                                    format(game, player))
                            self.games[game].add(None, player, silent=True)
                            if player in self.player_timeouts:
                                if create_dt <= self.player_timeouts[player].added:
                                    continue  # skip updating timeout
                            self.player_timeouts[player] = PlayerTimeout( added=create_dt, messaged=False )
                        else:
                            if not player in skipped:
                                skipped.append(player)
                return (added, skipped)

            def _cleandb(*args):
                added, skipped = args[0]
                log.msg("Removing {1} players from previous game preparation: {0}".\
                        format(skipped, len(skipped)))
                def _doTransaction(txn, skipped):
                    for p in skipped:
                        txn.execute("""
                            DELETE FROM pickup_games_preparation
                            WHERE player = ?
                        """, (p,))
                    return txn.fetchall()
                def done(*args):
                    self.pypickupbot.fire('restore_added_players', added, skipped)
                return db.runInteraction(_doTransaction, skipped).addCallback(done, added, skipped)

            d = FetchedList.get_users(self.pypickupbot, self.pypickupbot.channel).get()
            return d.addCallback(_fireEvent).addCallback(_cleandb)
        db.runInteraction(_doTransaction).addCallback(done)

    def joinedHomeChannel(self):
        """when home channel joined, set topic"""
        if config.get('Pickup', 'topic'):
            self.topic = self.pypickupbot.topic.add('', Topic.GRAVITY_BEGINNING)
            #self.update_topic()

    def userRenamed(self, oldname, newname):
        """track user renames"""
        self.xonstat._rename_user(oldname, newname)
        self.all_games().rename(oldname, newname)
        for p in self.player_timeouts.keys():
            if p == oldname:
                self.player_timeouts[newname] = self.player_timeouts[oldname]
                del self.player_timeouts[oldname]
                self.update_pickup_preparation(oldname,newname)
        #for p in self.player_highlights.keys():
        #    if p == oldname:
        #        self.player_highlights[newname] = self.player_highlights[oldname]
        #        del self.player_highlights[oldname]

    def userLeft(self, user, channel, *args):
        """track quitters"""
        nick = user.split('!')[0]
        if channel == self.pypickupbot.channel:
            self.all_games().force_remove(nick)
            self.remove_timeout(nick)
            self.remove_pickup_preparation(nick)
        #    if nick in self.player_highlights:
        #        del self.player_highlights[nick]

    def userQuit(self, user, quitMessage):
        """track quitters"""
        nick = user.split('!')[0]
        self.xonstat._remove_user(user)
        self.all_games().force_remove(nick)
        self.remove_timeout(nick)
        self.remove_pickup_preparation(nick)
        #if nick in self.player_highlights:
        #    del self.player_highlights[nick]

    def userAddedUpToGame(self, user, games):
        self.update_timeout(user)
        self.add_pickup_preparation(user, [g.nick for g in games])
        #log.msg("Checking for highlight on {0} players".format(len(self.xonstat.players)))
        #for target,player in self.xonstat.players.items():
        #    highlight_games = self._check_highlight(player, games)
        #    if target != user.lower() and len(highlight_games) > 0:
        #        msg = config.get('Pickup highlights', 'added up').decode("string-escape")%\
        #                {
        #                    'nick': user,
        #                    'channel': self.pypickupbot.channel,
        #                    'games': ", ".join([g.nick for g in highlight_games]),
        #                }
        #        self.pypickupbot.pmsg(target, msg)

    def userRemovedFromGame(self, user, games):
        self.remove_timeout(user)
        self.remove_pickup_preparation(user, [g.nick for g in games])
        #log.msg("Checking for highlight on {0} players".format(len(self.xonstat.players)))
        #for target,player in self.xonstat.players.items():
        #    highlight_games = self._check_highlight(player, games)
        #    if target != user.lower() and len(highlight_games) > 0:
        #        msg = config.get('Pickup highlights', 'removed').decode("string-escape")%\
        #                {
        #                    'nick': user,
        #                    'channel': self.pypickupbot.channel,
        #                    #'games': ", ".join([g.nick for g in highlight_games]),
        #                }
        #        self.pypickupbot.pmsg(target, msg)

    def update_timeout(self, nick, force = True):
        if force or nick in self.player_timeouts:
            self.player_timeouts[nick] = PlayerTimeout( added=time(), messaged=False )
            return True
        return False

    def remove_timeout(self, nick):
        if nick in self.player_timeouts:
            del self.player_timeouts[nick]
            return True
        return False

    def add_pickup_preparation(self, nick, games):
        def _doTransaction(txn, nick, games):
            tm = itime()
            for g in games:
                txn.execute("""
                    INSERT INTO pickup_games_preparation(game, player, time)
                    VALUES (:game, :player, :time)
                    """, (g, nick, tm,))
            return txn.fetchall()
        return db.runInteraction(_doTransaction, nick, games)

    def remove_pickup_preparation(self, nick, games=None):
        if not games:
            games = [ g.nick for g in self.all_games().games ]
        def _doTransaction(txn, nick, games):
            for g in games:
                txn.execute("""
                    DELETE FROM pickup_games_preparation
                    WHERE game = ? AND player = ?
                    """, (g, nick,))
            return txn.fetchall()
        return db.runInteraction(_doTransaction, nick, games)

    def remove_pickup_preparation_multi(self, nicks):
        def _doTransaction(txn, nicks):
            for n in nicks:
                txn.execute("""
                    DELETE FROM pickup_games_preparation
                    WHERE player = ?
                    """, (n,))
            return txn.fetchall()
        return db.runInteraction(_doTransaction, nicks)

    def update_pickup_preparation(self, oldnick, nick):
        d = db.runOperation("""
                UPDATE pickup_games_preparation
                SET player = ?
                WHERE player = ?
                """, (nick, oldnick,))
        return d

    def pickupGameStarting(self, game, players, captains):
        self.game_starting = True
        log.msg("Checking for highlight on {0} players".format(len(self.xonstat.players)))
        for target,player in self.xonstat.players.items():
            if not target in players and len(self._check_highlight(player, [game])) > 0:
                self.pypickupbot.pmsg(target,
                    config.get('Pickup highlights', 'game starting').decode("string-escape")%\
                        {
                            'game': game.nick,
                            'channel': self.pypickupbot.channel,
                        }
                    )

    def pickupGameStarted(self, game, players, captains):
        self.game_starting = False
        # Remove players whose game is starting from timeouts
        for p in players:
            self.remove_timeout(p)
        # Remove players from "preparation" table
        def done(*args):
            log.msg("{0} pickup game started - players: {1} [captains: {2}]".format(game, ",".join(players), ",".join(captains)))
        self.remove_pickup_preparation_multi(players).addCallback(done)

    def gameReadyMessages(self, players, teams, pmsg):
        delay = float(config.get('Pickup messages', 'game ready message delay'))
        sleep(delay)
        if config.getboolean("Pickup", "PM each player on start") and len(pmsg) > 0:
            for player in players:
                self.pypickupbot.pmsg(player.nick, "{0}: {1}".format(player.nick, pmsg))

    def restoredAddedPlayers(self, added, skipped):
        if len(added) > 0:
            msg = config.get('Pickup messages', 'restored players').decode('string-escape')
        else:
            msg = config.get('Pickup messages', 'restored players none').decode('string-escape')
        if len(skipped) > 0:
            msg += config.get('Pickup messages', 'restored players skipped').decode('string-escape')
        self.pypickupbot.cmsg(msg % \
            {
                'added':        ", ".join(added),
                'skipped':      ", ".join(skipped),
                'num_added':    len(added),
                'num_skipped':  len(skipped),
            })

    def _timeout_loop(self):
        """timeout loop called in a separate thread, checks timeouts of all active players"""
        log.msg("Starting timeout-check loop")
        check_interval = float(config.get('Pickup', 'renew check interval'))
        while True:
            self._check_timeout()
            sleep( check_interval )

    def _check_highlight(self, player, games):
        #print "Checking highlight for", player.nick, "on games:", [ g.nick for g in games ]
        highlight_games = []
        for game in games:
            m = _game_regex.match(game.nick)
            if not m:
                continue
            p,p1,p2,gt = m.groups()
            #print game, gt, player.subscriptions
            if gt in player.subscriptions:
                highlight_games.append(game)
        return highlight_games

    def _check_timeout(self):
        """check timeouts of all active players"""
        if len(self.player_timeouts) == 0:
            return

        # Make sure that only players who have added up are checked
        activeplayers = []
        for g in self.games.values():
            for p in g.players:
                if not p in activeplayers:
                    activeplayers.append(p)

        if len(activeplayers) == 0:
            return

        for p in self.player_timeouts.keys():
            if not p in activeplayers:
                del self.player_timeouts[p]

        now = time()
        log.msg("Checking timeout for {0} players at {1}: {2}".\
            format(len(self.player_timeouts), asctime(localtime(now)), ", ".join(self.player_timeouts.keys())))

        pre_timeout  = int(config.get('Pickup', 'renew pre-timeout'))
        full_timeout = int(config.get('Pickup', 'renew timeout'))
        removed = []
        for nick,info in self.player_timeouts.items():
            delta = now - info.added
            if (not info.messaged) and delta >= pre_timeout and delta < full_timeout:
                self.player_timeouts[nick] = PlayerTimeout(added=info.added,messaged=True)  # set 'messaged' to True
                pmsg = config.get('Pickup', 'timeout please renew').decode('string-escape')%\
                {
                    'added_at':     ceil(delta/60.0),
                    'timeout_delta':ceil((full_timeout-delta)/60.0),
                    'prefix':       config.get('Bot', 'command prefix'),
                }
                self.pypickupbot.pmsg(nick, pmsg)
            elif delta >= full_timeout:
                self.all_games().force_remove(nick)
                self.remove_timeout(nick)
                pmsg = config.get('Pickup', 'timeout you have been removed').decode('string-escape')%\
                {
                    'added_at':     ceil(delta/60.0),
                    'timeout':      ceil(full_timeout/60.0),
                    'prefix':       config.get('Bot', 'command prefix'),
                }
                self.pypickupbot.pmsg(nick, pmsg)
                removed.append(nick)
        if len(removed) > 0:
            def done(*args):
                log.msg("Removed {0} players from pickups: {1}".format(len(removed), ",".join(removed)))
            self.remove_pickup_preparation_multi(removed).addCallback(done)

    commands = {
        'add':              (add,           COMMAND.NOT_FROM_PM | COMMAND.NOT_FROM_OUTSIDE),
        'remove':           (remove,        COMMAND.NOT_FROM_PM | COMMAND.NOT_FROM_OUTSIDE),
        'renew':            (renew,         COMMAND.NOT_FROM_PM | COMMAND.NOT_FROM_OUTSIDE),
        'who':              (who,           0),
        'pickups':          (pickups,       0),

        'subscribe':        (subscribe,     0),
        'unsubscribe':      (unsubscribe,   0),

        'promote':          (promote,       COMMAND.NOT_FROM_PM | COMMAND.NOT_FROM_OUTSIDE),
        'pull':             (pull,          COMMAND.NOT_FROM_PM | COMMAND.NOT_FROM_OUTSIDE | COMMAND.ADMIN),
        'start':            (force_start,   COMMAND.NOT_FROM_PM | COMMAND.NOT_FROM_OUTSIDE | COMMAND.ADMIN),
        'abort':            (abort,         COMMAND.NOT_FROM_PM | COMMAND.NOT_FROM_OUTSIDE | COMMAND.ADMIN),

        'register':         (register,      COMMAND.NOT_FROM_PM),
        'whois':            (whois,         0),
        'info':             (playerInfo,    0),
        'player':           (playerExists,  0),
        'listplayers':      (listPlayers,   0),
        'searchplayers':    (searchPlayers, 0),
        'setskill':         (setSkill,      COMMAND.NOT_FROM_PM | COMMAND.NOT_FROM_OUTSIDE | COMMAND.ADMIN),

        'removeplayer':     (removePlayer,  COMMAND.NOT_FROM_PM),
        'clearplayers':     (clearPlayers,  COMMAND.NOT_FROM_PM | COMMAND.ADMIN),
        'purgeplayers':     (purgePlayers,  COMMAND.NOT_FROM_PM | COMMAND.ADMIN),

        'makematch':        (makematch,     COMMAND.ADMIN),
        'checkin':          (checkin,       COMMAND.ADMIN),
        'checkout':         (checkout,      COMMAND.ADMIN),
        'matchlist':        (matchlist,     COMMAND.ADMIN),
        }

    eventhandlers = {
        'joinedHomeChannel':    joinedHomeChannel,
        'userRenamed':          userRenamed,
        'userLeft':             userLeft,
        'userKicked':           userLeft,
        'userQuit':             userQuit,
        'pickup_game_starting': pickupGameStarting,
        'pickup_game_started':  pickupGameStarted,
        'pickup_game_ready':    gameReadyMessages,
        'user_added_up':        userAddedUpToGame,
        'user_removed':         userRemovedFromGame,
        'restore_added_players':    restoredAddedPlayers,
    }

xonstat_pickup = SimpleModuleFactory(XonstatPickupBot)

